package com.daddydsbbq.Model.RequestModel;

public class GetUserRegisrationIdModel {
    final  String username;
    final String email;
    final String first_name;
    final String last_name;
    final String password;
    final String verified;
    final  String restaurant_id;


    public GetUserRegisrationIdModel(String username, String email, String first_name, String last_name, String password, String verified, String restaurant_id) {
        this.username = username;
        this.email = email;
        this.first_name = first_name;
        this.last_name = last_name;
        this.password = password;
        this.verified = verified;
        this.restaurant_id = restaurant_id;
    }
}
