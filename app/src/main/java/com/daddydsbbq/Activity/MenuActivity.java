package com.daddydsbbq.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.menu.MenuAdapter;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.daddydsbbq.Adapters.MenuListAdapter;
import com.daddydsbbq.ApiUtilities.RetrofitClient;
import com.daddydsbbq.Model.ResponseModel.MenuDataModel;
import com.daddydsbbq.Others.AppUtilities;
import com.daddydsbbq.Others.CommonUtilities;
import com.daddydsbbq.Others.Messages;
import com.daddydsbbq.Others.SPUtilities;
import com.daddydsbbq.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MenuActivity extends AppCompatActivity {

    private  String TAG = MenuActivity.class.getName();
    private Context mContext = MenuActivity.this;
    private Dialog loadingDialog;
    private  String restaurant_status;
    private int cat_id;


    ArrayList<MenuDataModel> mMenuDataModel;
    MenuListAdapter mMenuAdapter;
    RecyclerView rvMenu;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        mMenuDataModel = new ArrayList<>();
        rvMenu=findViewById(R.id.menu_rv);
        rvMenu.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));

        cat_id = getIntent().getIntExtra("cat_id", 0);
       // Toast.makeText(mContext, "Clicked:- "+cat_id, Toast.LENGTH_SHORT).show();


        findViewById(R.id.menu_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        findViewById(R.id.menu_viewcart_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, CartActivity.class);
                startActivity(intent);
            }
        });


        if(CommonUtilities.isOnline(mContext)) {
            getCategoryMenuList(cat_id);
        }else
        {
            CommonUtilities.showCommonTextAlert(mContext,Messages.INTERNET_ALERT);
        }
    }


    @Override
    protected void onResume() {
        super.onResume();

        if(CommonUtilities.isOnline(mContext)) {
            getCartCount();
        }else
        {
            CommonUtilities.showCommonTextAlert(mContext,Messages.INTERNET_ALERT);
        }

    }
    private void getCategoryMenuList(int category_id)
    {

        loadingDialog = ProgressDialog.show(mContext, "Please wait", "Loading...");

        String token = SPUtilities.getToken(mContext);
        Log.e("ProfileActivity", "Token: "+token);

        Call<ResponseBody> call = RetrofitClient.getmInstance().getApi().getCategoryMenuList("Token "+token, AppUtilities.RestaurantGlobalid,category_id,"ACTIVE",1);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                loadingDialog.dismiss();
                int responseCode = response.code();
                Log.e("TAG", "ResponseCode: "+responseCode);

                if(responseCode == 200)
                {
                    try {
                        String s = response.body().string();
                        Log.e("TAG", "Response: "+s);
                        JSONObject jsonObject=new JSONObject(s);
                        JSONArray resultsArray = jsonObject.getJSONArray("results");

                        if(resultsArray.length() <= 0)
                        {

                            Log.e("TAG", "No best seller: ");

                        }
                        else
                        {
                            Log.e(TAG, "Best Seller: "+resultsArray);
                            for(int i=0;i<resultsArray.length();i++)
                            {
                                JSONObject json=resultsArray.getJSONObject(i);

                                MenuDataModel object =new MenuDataModel();
                                object.setProduct_id(json.getInt("product_id"));
                                object.setPrice(json.getDouble("price"));
                                object.setProduct_name(json.getString("product_name"));
                                object.setProduct_url(json.getString("product_url"));
                                object.setDesc(json.getString("extra"));


                                mMenuDataModel.add(object);

                            }

                            setMenuAdapter();



                        }

                    }
                    catch (NullPointerException e){}
                    catch (IOException e) { e.printStackTrace(); }
                    catch (JSONException e) {e.printStackTrace();}

                }
                else if(responseCode == 401)
                {
                    Log.e(TAG, "ResponseError: "+responseCode);
                    CommonUtilities.sessionExpiredAlert(mContext);

                }
                else if(responseCode == 500)
                {
                    Log.e(TAG, "ResponseError: "+responseCode);

                    try {
                        String s = response.errorBody().string();
                        JSONObject jsonObject = new JSONObject(s);
                        Log.e("TAG", "ResponseError object: "+jsonObject);
                        String msg = jsonObject.getString("msg");
                        Log.e("TAG", "Msg: "+msg);
                        CommonUtilities.showCommonTextAlert(mContext, msg);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
                else  {

                    CommonUtilities.showCommonTextAlert(mContext, Messages.UNKNOWN_RESPONSE);

                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                Log.e(TAG, "onFailure: "+t.getMessage());
                loadingDialog.dismiss();
            }
        });
    }


    private void setMenuAdapter() {

        mMenuAdapter = new MenuListAdapter(mMenuDataModel, this);
        rvMenu.setAdapter(mMenuAdapter);
        mMenuAdapter.notifyDataSetChanged();
        // let your adapter know about the changes and reload view.


    }

    //get cart count
    private void getCartCount()
    {
        //  String token = SharePreferenceUtilities.getToken(mContext);
        String graphqlToken = AppUtilities.globGraphQlToken;
        String customerid = String.valueOf(SPUtilities.getCustomerId(mContext));
        String restid = AppUtilities.RestaurantGlobalid;
        MediaType mediaType = MediaType.parse("application/json");
        String ss  = "{\"query\":\"query cartItemCount ($token: String, $customerId: Int, $restaurantId: Int) {\\n  cartItemCount (token : $token,customerId: $customerId, restaurantId: $restaurantId) {\\n    count\\n  }\\n}\",\"variables\":{\"token\":\"0o6jcui8mfhmp56we69kcmu5rkejtock\",\"customerId\":"+customerid+",\"restaurantId\":"+restid+"}}";

        //  String s2 = "{\"query\":\"query cartItemCount (\$token: String, \$customerId: Int,\$restaurantId: Int) {\\n  cartItemCount (token : \$token,customerId: \$customerId,restaurantId: \$restaurantId ) {\\n    count\\n  }\\n}\",\"variables\":{\"token\":\"0o6jcui8mfhmp56we69kcmu5rkejtock\",\"customerId\":"+customerid+",\"restaurantId\":\"$restid\"}}";
        RequestBody requestBody = RequestBody.create(mediaType,ss);

        Log.e(TAG, "getCartCount request string: "+ss);

        Call<ResponseBody> call = RetrofitClient.getmInstance().getApi().getCartCount(requestBody);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                int responseCode = response.code();
                Log.e("TAG", "ResponseCode: "+responseCode);

                if(responseCode == 200)
                {
                    try {
                        String s = response.body().string();
                        // Log.e("TAG", "Response: "+s);
                        JSONObject jsonObject=new JSONObject(s);
                        JSONObject dataObject = jsonObject.getJSONObject("data");
                        JSONObject countObject = dataObject.getJSONObject("cartItemCount");
                        int cartCount = countObject.getInt("count");
                        TextView textView = findViewById(R.id.menu_itemcount_text);
                        textView.setText(""+cartCount+" Item");
                        SPUtilities.setCartCount(mContext,cartCount);

                        Log.e(TAG, "Cart Count Response: "+s);

                    }
                    catch (NullPointerException e){}
                    catch (IOException e) { e.printStackTrace(); }
                    catch (JSONException e) {e.printStackTrace();}

                }
                else if(responseCode == 401)
                {
                    Log.e("TAG", "ResponseError: "+responseCode);
                    CommonUtilities.sessionExpiredAlert(mContext);

                }
                else if(responseCode == 400)

                {
                    Log.e(TAG, "ResponseError: "+responseCode);

                    try {
                        String s = response.errorBody().string();
                        JSONObject jsonObject = new JSONObject(s);
                        Log.e(TAG, "ResponseError object: "+jsonObject);
                        String msg = jsonObject.getString("msg");
                        Log.e(TAG, "Msg: "+msg);
                        CommonUtilities.showCommonTextAlert(mContext, msg);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
                else if(responseCode == 500)
                {
                    Log.e("TAG", "ResponseError: "+responseCode);

                    try {
                        String s = response.errorBody().string();
                        JSONObject jsonObject = new JSONObject(s);
                        Log.e("TAG", "ResponseError object: "+jsonObject);
                        String msg = jsonObject.getString("msg");
                        Log.e("TAG", "Msg: "+msg);
                        CommonUtilities.showCommonTextAlert(mContext, msg);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
                else  {

                    CommonUtilities.showCommonTextAlert(mContext, "The system is temporarily unavailable. Please try again later");

                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                Log.e("TAG", "onFailure: "+t.getMessage());
            }
        });
    }
}