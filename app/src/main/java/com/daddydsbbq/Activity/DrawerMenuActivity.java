package com.daddydsbbq.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import com.daddydsbbq.Adapters.DrawerAdapter;
import com.daddydsbbq.Others.SPUtilities;
import com.daddydsbbq.R;

public class DrawerMenuActivity extends AppCompatActivity {

    GridView gridView;
    Context mContext;
    private  String TAG = DrawerMenuActivity.class.getName();
    android.app.AlertDialog alertDialog;


    String[] menuArray;
    //= {"Menu", "Profile", "Order list", "Cart","About us", "Contact us", "Logout"};
    DrawerAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drawer_menu);
        mContext = DrawerMenuActivity.this;

        boolean loginStatus = SPUtilities.getLoginStatus(mContext);
        String userType = SPUtilities.getUserType(mContext);

        if(loginStatus==true)
        {
            if(userType.equals("REGISTERED"))
            {
                  menuArray = new String[]{"Menu", "Profile", "Order list", "Cart", "About us", "Contact us", "Logout"};


            }
            else
            {
                menuArray = new String[]{"Menu", "Order list", "Cart","About us", "Contact us", "Logout"};
            }
        }
        else
        {

            menuArray = new String[]{"Menu", "About us", "Contact us", "Login"};

        }







        gridView = findViewById(R.id.drawer_gridview);
        adapter = new DrawerAdapter(mContext, menuArray);
        gridView.setAdapter(adapter);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (menuArray[position].equals("Menu"))
                {
                    Intent intent = new Intent(mContext, HomeActivity.class);
               startActivity(intent);
                } else if (menuArray[position].equals("Profile")) {
                    Intent intent = new Intent(mContext, ProfileActivity.class);
              startActivity(intent);

                }else if (menuArray[position].equals("Order list")) {
                    Intent intent = new Intent(mContext, ProfileActivity.class);
             startActivity(intent);

                }else if (menuArray[position].equals("Cart")) {
                    Intent intent = new Intent(mContext, CartActivity.class);
               startActivity(intent);

                }else if (menuArray[position].equals("About us")) {
                    Intent intent = new Intent(mContext, AboutActivity.class);
               startActivity(intent);
                }else if (menuArray[position].equals("Contact us")) {
                    Intent intent = new Intent(mContext, ContactActivity.class);
                 startActivity(intent);
                }else if (menuArray[position].equals("Logout")) {
                    logoutAlert();
                }else if (menuArray[position].equals("Login")) {
                    Intent intent = new Intent(mContext, LoginActivity.class);
                    startActivity(intent);
                }
                finish();

            }
        });
    }


    //Logout alert
    private void logoutAlert()
    {
        LayoutInflater li = LayoutInflater.from(mContext);
        final View promptsView = li.inflate(R.layout.common_alert_layout2, null);
        android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(mContext);
        // set prompts.xml to alertdialog builder
        alertDialogBuilder.setView(promptsView);
        final TextView titalText = promptsView
                .findViewById(R.id.alert_msg);
        titalText.setText("Are you sure you want to logout?");


        promptsView.findViewById(R.id.btn_ok).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.cancel();
                SPUtilities.clearSharedPreference(mContext);
                Intent intent = new Intent(mContext, HomeActivity.class);
                startActivity(intent);
                finish();
            }
        });

        promptsView.findViewById(R.id.btn_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.cancel();
            }
        });
        // create alert dialog
        alertDialog = alertDialogBuilder.create();
        alertDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        alertDialog.show();




    }

}