package com.daddydsbbq.Model.RequestModel;

public class UserRegistrationModel {
    final  String customer_id;
    final String last_access;
    final String extra;
    final String salutation;
    final String phone_number;
    final String first_name;
    final  String last_name;
    final  String email;

    public UserRegistrationModel(String customer_id, String last_access, String extra, String salutation, String phone_number, String first_name, String last_name, String email) {
        this.customer_id = customer_id;
        this.last_access = last_access;
        this.extra = extra;
        this.salutation = salutation;
        this.phone_number = phone_number;
        this.first_name = first_name;
        this.last_name = last_name;
        this.email = email;
    }
}
