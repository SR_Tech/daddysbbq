package com.daddydsbbq.Others;

public class Messages {

    public static String SESSION_EXPIRED_MSG = "Session expired. Please login again.";
    public static String CART_NOT_CREATED = "Cart not created";
    public static String REGISTRATION_SUCCESSFULL_TITAL = "Registration has been done successfully.";
    public static String REGISTRATION_SUCCESSFULL = "We sent you an email for account verification.";
    public static String FORGOT_PASSWORD = "Password reset email has been sent.";
    public static String UNKNOWN_RESPONSE = "The system is temporarily unavailable. Please try again later.";



    public static String INTERNET_ALERT = "No internet connection";
    public static String OTP_VERIFICATION_FAILED = "User verification failed";



}
