package com.daddydsbbq.Others;

import android.content.Context;
import android.content.SharedPreferences;

import org.json.JSONObject;

import static android.content.Context.MODE_PRIVATE;

public class SPUtilities {
    ;

    // public static Context mContext;
    public static String mSPreferenceName = "MyPref";


    public static String AUTHORIZATION_TOKEN = "authorization_token";
    public static String LAST_RESPONSE_CODE = "last_response_code";
    public static String CART_ID = "cart_id";
    public static String STOREID_WRT_CART = "storeIdWRTCart";

    public static String CATEGORY_ID_WRT_CART = "category_id_wrt_Cart";
    public static String ZIPCODE = "zipcode";
    public static String CART_COUNT = "cart_count";
    public static String COUNTRY = "country";
    public static String CURRENCY_TYPE = "currency_type";


    /*
     public static String  COUNTRY = "country";
    public static String SUB_TOTAL = "sub_total";
    public static String NO_TAX_TOTAL = "no_tax_total";
    public static String TAX = "tax";
    public static String TIP = "tip";
    public static String SERVICE_FEE = "service_fee";
    public static String DISCOUNT = "discount";
    public static String TOTAL = "total";
     public static String  COOKIES = "cookies";
    public static String  CSRF_TOKEN = "csrf_token";*/


    //SP Keys

    public static String TOKEN = "token";
    public static String CUSTOMER_ID = "custId";
    //  public static String CUSTOMER_TOKEN = "custToken";
    public static String USERTYPE = "Usertype";
    public static String USERLOG = "reguser";


    public static void clearSharedPreference(Context mContext) {
        SharedPreferences.Editor pref = mContext.getSharedPreferences(mSPreferenceName, 0).edit();
        pref.clear();
        pref.commit();
    }

    public static void setToken(Context mContext, String val) {
        SharedPreferences.Editor editor = mContext.getSharedPreferences(mSPreferenceName, MODE_PRIVATE).edit();
        editor.putString("token", val);
        editor.commit();
    }

    public static String getToken(Context mContext) {
        SharedPreferences prefs = mContext.getSharedPreferences(mSPreferenceName, MODE_PRIVATE);
        String value = prefs.getString("token", null);
        return value;
    }


    public static void setLoginStatus(Context mContext, boolean value) {
        SharedPreferences.Editor editor = mContext.getSharedPreferences(mSPreferenceName, MODE_PRIVATE).edit();
        editor.putBoolean("loginStatus", value);
        //editor.putString("getCurrentUserId", value);
        editor.commit();
    }

    public static boolean getLoginStatus(Context mContext) {
        SharedPreferences prefs = mContext.getSharedPreferences(mSPreferenceName, MODE_PRIVATE);
        boolean value = prefs.getBoolean("loginStatus", false);
        return value;
    }

    public static void setRestaurantStatus(Context mContext, String val) {
        SharedPreferences.Editor editor = mContext.getSharedPreferences(mSPreferenceName, MODE_PRIVATE).edit();
        editor.putString("rest_status", val);
        editor.commit();
    }

    public static String getRestaurantStatus(Context mContext) {
        SharedPreferences prefs = mContext.getSharedPreferences(mSPreferenceName, MODE_PRIVATE);
        String value = prefs.getString("rest_status", null);
        return value;
    }

    public static void setCustomerId(Context mContext, int val) {
        SharedPreferences.Editor editor = mContext.getSharedPreferences(mSPreferenceName, MODE_PRIVATE).edit();
        editor.putInt("custid", val);
        editor.commit();
    }

    public static int getCustomerId(Context mContext) {
        SharedPreferences prefs = mContext.getSharedPreferences(mSPreferenceName, MODE_PRIVATE);
        int value = prefs.getInt("custid", 0);
        return value;
    }


    public static void setUserType(Context mContext, String val) {
        SharedPreferences.Editor editor = mContext.getSharedPreferences(mSPreferenceName, MODE_PRIVATE).edit();
        editor.putString("setUserId", val);
        editor.commit();
    }

    public static String getUserType(Context mContext) {
        SharedPreferences prefs = mContext.getSharedPreferences(mSPreferenceName, MODE_PRIVATE);
        String value = prefs.getString("setUserId", null);
        return value;
    }

    public static void setUsername(Context mContext, String val) {
        SharedPreferences.Editor editor = mContext.getSharedPreferences(mSPreferenceName, MODE_PRIVATE).edit();
        editor.putString("username", val);
        editor.commit();
    }

    public static String getUsername(Context mContext) {
        SharedPreferences prefs = mContext.getSharedPreferences(mSPreferenceName, MODE_PRIVATE);
        String value = prefs.getString("username", null);
        return value;
    }

    public static void setCartId(Context mContext, int val) {
        SharedPreferences.Editor editor = mContext.getSharedPreferences(mSPreferenceName, MODE_PRIVATE).edit();
        editor.putInt("cart_id", val);
        editor.commit();
    }

    public static int getCartId(Context mContext) {
        SharedPreferences prefs = mContext.getSharedPreferences(mSPreferenceName, MODE_PRIVATE);
        int value = prefs.getInt("cart_id", 0);
        return value;
    }

    public static void setStoreidWrtCart(Context mContext, int val) {
        SharedPreferences.Editor editor = mContext.getSharedPreferences(mSPreferenceName, MODE_PRIVATE).edit();
        editor.putInt("store_id_wrt_cart", val);
        editor.commit();
    }

    public static int getStoreidWrtCart(Context mContext) {
        SharedPreferences prefs = mContext.getSharedPreferences(mSPreferenceName, MODE_PRIVATE);
        int value = prefs.getInt("store_id_wrt_cart", 0);
        return value;
    }

    public static void setCartCount(Context mContext, int val) {
        SharedPreferences.Editor editor = mContext.getSharedPreferences(mSPreferenceName, MODE_PRIVATE).edit();
        editor.putInt("cart_count", val);
        editor.commit();
    }

    public static int getCartCount(Context mContext) {
        SharedPreferences prefs = mContext.getSharedPreferences(mSPreferenceName, MODE_PRIVATE);
        int value = prefs.getInt("cart_count", 0);
        return value;
    }


    public static void setCartTotal(Context mContext, int val) {
        SharedPreferences.Editor editor = mContext.getSharedPreferences(mSPreferenceName, MODE_PRIVATE).edit();
        editor.putInt("cart_total", val);
        editor.commit();
    }

    public static int getCartTotal(Context mContext) {
        SharedPreferences prefs = mContext.getSharedPreferences(mSPreferenceName, MODE_PRIVATE);
        int value = prefs.getInt("cart_total", 0);
        return value;
    }

    public static void setBillingAddress(Context mContext, JSONObject jsonObject) {

        String jsonString = jsonObject.toString();
        SharedPreferences.Editor editor = mContext.getSharedPreferences(mSPreferenceName, MODE_PRIVATE).edit();
        editor.putString("billingAddress", jsonString);
        editor.commit();
    }

    public static String getBillingAddress(Context mContext) {

            SharedPreferences prefs = mContext.getSharedPreferences(mSPreferenceName, MODE_PRIVATE);
            String jsonString = prefs.getString("billingAddress", null);
            return jsonString;

    }



    public static void setRestaurantId(Context mContext, String val) {
        SharedPreferences.Editor editor = mContext.getSharedPreferences(mSPreferenceName, MODE_PRIVATE).edit();
        editor.putString("rest_id", val);
        editor.commit();
    }


    public static String getRestaurantId(Context mContext) {
        SharedPreferences prefs = mContext.getSharedPreferences(mSPreferenceName, MODE_PRIVATE);
        String value = prefs.getString("rest_id", null);
        return value;
    }

    public static void setRestaurantCity(Context mContext, String val) {
        SharedPreferences.Editor editor = mContext.getSharedPreferences(mSPreferenceName, MODE_PRIVATE).edit();
        editor.putString("rest_city", val);
        editor.commit();
    }

    public static String getRestaurantCity(Context mContext) {
        SharedPreferences prefs = mContext.getSharedPreferences(mSPreferenceName, MODE_PRIVATE);
        String value = prefs.getString("rest_city", null);
        return value;
    }

}


//    public static void clear(Context context) {
//        SharedPreferences pref = context.getSharedPreferences("MyPref", 0);
//        SharedPreferences.Editor editor = pref.edit();
//        // Clearing all user data from Shared Preferences
//        editor.clear();
//        editor.commit();
//
//    }



//    //Store string using Shared Preference
//    public static void setSPstring(Context mContext, String key, String value) {
//        SharedPreferences pref = mContext.getSharedPreferences("MyPref", 0);
//        SharedPreferences.Editor editor = pref.edit();
//        editor.putString(key, value);
//        editor.commit();
//    }
//
//    //Store integer using Shared Preference
//    public static void setSPinteger(Context mContext, String key, int value) {
//        SharedPreferences pref = mContext.getSharedPreferences("MyPref", 0);
//        SharedPreferences.Editor editor = pref.edit();
//        editor.putInt(key, value);
//        editor.commit();
//    }




//    //get int value from Shared Preference
//    public static int getSPintValue(Context mContext, String key) {
//        SharedPreferences sharedPreferences = mContext.getSharedPreferences("MyPref", Context.MODE_PRIVATE);
//        int value = sharedPreferences.getInt(key, 0);
//        return value;
//    }
//
//    //get string value from Shared Preference
//    public static String getSPstringValue(Context mContext, String key) {
//        SharedPreferences sharedPreferences = mContext.getSharedPreferences("MyPref", Context.MODE_PRIVATE);
//        String value = sharedPreferences.getString(key, "");
//        return value;
//    }
//
//
//    //Store boolean using Shared Preference
//    public static void setSPboolean(Context mContext, String key, boolean value) {
//        SharedPreferences pref = mContext.getSharedPreferences("MyPref", 0);
//        SharedPreferences.Editor editor = pref.edit();
//        editor.putBoolean(key, value);
//        editor.commit();
//    }
//
//    //get boolean value from Shared Preference
//    public static boolean getSPbooleanValue(Context mContext, String key) {
//        SharedPreferences sharedPreferences = mContext.getSharedPreferences("MyPref", Context.MODE_PRIVATE);
//        boolean value = sharedPreferences.getBoolean(key, false);
//        return value;
//    }
//
//    public static void clearSP(Context mContext)
//    {
//        SharedPreferences settings = mContext.getSharedPreferences("MyPref", Context.MODE_PRIVATE);
//        settings.edit().clear().commit();
//    }

    /*public static  void setArrayList(Context context, String key, ArrayList<String> list){
        SharedPreferences sharedPreferences=context.getSharedPreferences("MyPref",Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        Gson gson = new Gson();
        String json = gson.toJson(list);
        editor.putString(key, json);
        editor.apply();     // This line is IMPORTANT !!!
    }

    public static ArrayList<String> getArrayList(Context context,String key){
        SharedPreferences sharedPreferences=context.getSharedPreferences("MyPref",Context.MODE_PRIVATE);
        Gson gson = new Gson();
        String json = sharedPreferences.getString(key, null);
        Type type = new TypeToken<ArrayList<String>>() {}.getType();
        return gson.fromJson(json, type);
    }
*/