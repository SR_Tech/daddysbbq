package com.daddydsbbq.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.daddydsbbq.ApiUtilities.RetrofitClient;
import com.daddydsbbq.Others.CommonUtilities;
import com.daddydsbbq.Others.Messages;
import com.daddydsbbq.Others.SPUtilities;
import com.daddydsbbq.R;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MenuDetailActivity extends AppCompatActivity {


    private String TAG = MenuDetailActivity.class.getName();
    private Context mContext = MenuDetailActivity.this;

    private ImageView menuImg;
    TextView tvMenuName,tvMenuDesc,tvMenuPrice, tvQuantity,tvTotalCost1,tvTotalCost2,tv_add_ingredient_text;
    int mMenuQuantity = 1, mMenuId;
    double mTotalCost = 0.0;
    double ingredient_cost = 0.0;
    String mMenuName,mMenuPrice,mMenuDesc,mMenuUrl;
    private Dialog loadingDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_detail);
        setOutletsIds();
    }





    private void setOutletsIds()
    {
        menuImg = findViewById(R.id.menu_detail_menu_img);
        tvMenuName = findViewById(R.id.menu_detail_menu_name_text);
        tvMenuDesc = findViewById(R.id.menu_detail_menu_desc_text);
        tvMenuPrice = findViewById(R.id.menu_detail_price_text);
        tvQuantity = findViewById(R.id.menu_detail_quantity_text);
        tvTotalCost1 = findViewById(R.id.menu_detail_amount_text);
        tvTotalCost2 = findViewById(R.id.menu_detail_total_cost_text);

       // tv_add_ingredient_text = findViewById(R.id.add_ingredient_text);
      //  tv_add_ingredient_text.setVisibility(View.GONE);
       // rvIngredient = findViewById(R.id.menu_detail_rv_ingredient);
       // rvIngredient.setVisibility(View.GONE);

        getBundleVlaues();


        findViewById(R.id.menu_detail_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        findViewById(R.id.menu_detail_add_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                addButtonClicked();
            }
        });

        findViewById(R.id.menu_detail_minus_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              minusButtonClicked();

            }
        });

        findViewById(R.id.menu_detail_add_to_cart_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addMenutoCart();
            }
        });


    }

    private void getBundleVlaues()
    {
        mMenuName = getIntent().getStringExtra("product_name");
        mMenuId = getIntent().getIntExtra("product_id",0);
        mMenuDesc = getIntent().getStringExtra("product_desc");
        mMenuPrice = getIntent().getStringExtra("product_price");
        mMenuUrl = getIntent().getStringExtra("product_url");


        mTotalCost = Double.parseDouble(mMenuPrice);
        tvMenuName.setText(mMenuName);
        tvMenuDesc.setText(mMenuDesc);
        tvMenuPrice.setText("$"+ CommonUtilities.getFormatedDoubleValue(Double.parseDouble(mMenuPrice)));
        tvQuantity.setText(""+mMenuQuantity);
        tvTotalCost1.setText("$"+CommonUtilities.getFormatedDoubleValue(Double.parseDouble(mMenuPrice)));
        tvTotalCost2.setText("$"+CommonUtilities.getFormatedDoubleValue(Double.parseDouble(mMenuPrice)));


        Glide.with(mContext).load(mMenuUrl).into(menuImg);
//                .placeholder(R.drawable.myplaceholder)


    }



    private void addButtonClicked(){

        mMenuQuantity++;
        mTotalCost =  (mMenuQuantity* Double.parseDouble(mMenuPrice)) ;

        tvQuantity.setText(""+mMenuQuantity);
        tvTotalCost1.setText("$"+CommonUtilities.getFormatedDoubleValue(mTotalCost));
        tvTotalCost2.setText("$"+CommonUtilities.getFormatedDoubleValue(mTotalCost));

    }

    private void minusButtonClicked() {

        if (mMenuQuantity > 1) {
            mMenuQuantity--;
            mTotalCost = mMenuQuantity * Double.parseDouble(mMenuPrice);
            tvQuantity.setText("" + mMenuQuantity);
            tvTotalCost1.setText("$" + CommonUtilities.getFormatedDoubleValue(mTotalCost));
            tvTotalCost2.setText("$" + CommonUtilities.getFormatedDoubleValue(mTotalCost));
        }


    }




    // add menu to cart
    private void addMenutoCart() {
        loadingDialog = ProgressDialog.show(mContext, "Please wait", "Loading...");
        String token = SPUtilities.getToken(mContext);
        int seq_no = SPUtilities.getCartCount(mContext);
        int cart_id = SPUtilities.getCartId(mContext);
        String time = CommonUtilities.getCurrentTime();

        JsonArray jsonArray = new JsonArray();

        try {
            JsonObject itemJsonObject = new JsonObject();
            itemJsonObject.addProperty("updated_at", time);
            itemJsonObject.addProperty("extra", "");
            itemJsonObject.addProperty("quantity", mMenuQuantity);
            itemJsonObject.addProperty("cart_id", cart_id);
            itemJsonObject.addProperty("product_id", mMenuId);
            itemJsonObject.addProperty("ingredient_id", 0);
            itemJsonObject.addProperty("sequence_id", seq_no);
            jsonArray.add(itemJsonObject);


           // Log.e(TAG, "addMenutoCart: mapSelectedIngredient-size - "+mapSelectedIngredient.size() );
          //  Log.e(TAG, "addMenutoCart: mapSelectedIngredient - "+mapSelectedIngredient );

//            JsonObject itemJsonObject2;
//            for(Map.Entry m : mapSelectedIngredient.entrySet())
//            {
//
//                String s = (String) m.getValue();
//                JSONObject jsonObject = new JSONObject(s);
//                JsonObject ingredientJsonObject = new JsonObject();
//                ingredientJsonObject.addProperty("updated_at", time);
//                ingredientJsonObject.addProperty("extra", "");
//                ingredientJsonObject.addProperty("quantity", 1);
//                ingredientJsonObject.addProperty("cart_id", cart_id);
//                ingredientJsonObject.addProperty("product_id", mMenuId);
//                ingredientJsonObject.addProperty("ingredient_id", jsonObject.getInt("ingredient_id"));
//                ingredientJsonObject.addProperty("sequence_id", seq_no);
//                jsonArray.add(ingredientJsonObject);
//
//
//            }


        }catch (final Exception e){
            Log.e(TAG, "Exception: ");
        }

        Log.e(TAG, "addMenutoCart: Inputs - "+jsonArray );



        Call<ResponseBody> call = RetrofitClient.getmInstance().getApi().addMenuToCart("Token "+token,
                jsonArray);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                loadingDialog.dismiss();

                int responseCode = response.code();
                Log.e("TAG", "ResponseCode: "+responseCode);

                if(responseCode == 201)
                {
                    Toast.makeText(mContext, "Product added successfully into the cart.", Toast.LENGTH_SHORT).show();

                    finish();
                }
                else if(responseCode == 401)
                {
                    loadingDialog.dismiss();

                    Log.e("TAG", "ResponseError: "+responseCode);
                    CommonUtilities.sessionExpiredAlert(mContext);


                }
                else if(responseCode == 500)
                {
                    Log.e("TAG", "ResponseError: "+responseCode);

                    try {
                        String s = response.errorBody().string();
                        JSONObject jsonObject = new JSONObject(s);
                        Log.e("TAG", "ResponseError object: "+jsonObject);
                        String msg = jsonObject.getString("msg");
                        Log.e("TAG", "Msg: "+msg);
                        CommonUtilities.showCommonTextAlert(mContext, msg);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
                else  {


                    CommonUtilities.showCommonTextAlert(mContext, Messages.UNKNOWN_RESPONSE);

                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                Log.e("TAG", "onFailure: "+t.getMessage());
                loadingDialog.dismiss();
            }
        });
    }

}