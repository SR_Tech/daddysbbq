package com.daddydsbbq.Model.RequestModel;

public class LoginModel {
    final String username;
    final String password;
    final String restaurant_id;

    public LoginModel(String username, String password, String restaurant_id) {
        this.username = username;
        this.password = password;
        this.restaurant_id = restaurant_id;
    }
}
