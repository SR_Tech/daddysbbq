package com.daddydsbbq.Model.RequestModel;

public class UpdateProfileModel {
    final String salutation;
    final String first_name;
    final String last_name;
    final String username;
    final String email;
    final String last_access;
    final String phone_number;


    public UpdateProfileModel(String salutation, String first_name, String last_name, String username, String email, String last_access, String phone_number) {
        this.salutation = salutation;
        this.first_name = first_name;
        this.last_name = last_name;
        this.username = username;
        this.email = email;
        this.last_access = last_access;
        this.phone_number = phone_number;
    }
}
