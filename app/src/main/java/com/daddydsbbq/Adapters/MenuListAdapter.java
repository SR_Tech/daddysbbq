package com.daddydsbbq.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.daddydsbbq.Activity.MenuDetailActivity;
import com.daddydsbbq.Model.ResponseModel.MenuDataModel;
import com.daddydsbbq.Others.CommonUtilities;
import com.daddydsbbq.R;

import java.util.ArrayList;

public class MenuListAdapter extends RecyclerView.Adapter<MenuListAdapter.ViewHolderClass>  {

    public ArrayList<MenuDataModel> mMenuDataModel;
    private Context mContext;
   // private IAddtoCartClickListner mIAddtoCartClickListner;


    public MenuListAdapter(ArrayList<MenuDataModel> mMenuDataModel, Context mContext) {

        this.mMenuDataModel = mMenuDataModel;
        this.mContext = mContext;

    }

//    public interface IAddtoCartClickListner
//    {
//        void onAddToCartClick(View view, int position);
//
//    }
//
//
//    public void setAddToCartClickListener(IAddtoCartClickListner mIAddtoCartClickListner) {
//        this.mIAddtoCartClickListner = mIAddtoCartClickListner;
//    }



    public class ViewHolderClass extends RecyclerView.ViewHolder {


        public ImageView product_imgview;
        public TextView product_name;
        public TextView product_price;
        public TextView product_desc;
        public Button add_to_cart;
        public CardView view;


        public ViewHolderClass(View itemView) {
            super(itemView);
            product_imgview = itemView.findViewById(R.id.menu_img);
            product_name = itemView.findViewById(R.id.menu_name);
            product_price = itemView.findViewById(R.id.menu_price);
            product_desc = itemView.findViewById(R.id.menu_desc);
            view = itemView.findViewById(R.id.card);

        }
    }

    @Override
    public ViewHolderClass onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        View view = inflater.inflate(R.layout.menu_layout,viewGroup,false);
        return new MenuListAdapter.ViewHolderClass(view);
    }


    @Override
    public int getItemCount() {
        return mMenuDataModel.size();
    }


    @Override
    public void onBindViewHolder(@NonNull MenuListAdapter.ViewHolderClass viewHolderClass, final int position) {

        Glide.with(mContext)
                .load(mMenuDataModel.get(position).getProduct_url())
//                .placeholder(R.drawable.myplaceholder)
                .error(R.drawable.no_img)
                .into(viewHolderClass.product_imgview);
        //viewHolderClass.product_imgview.setImageIcon();

        viewHolderClass.product_price.setText("$"+ mMenuDataModel.get(position).getPrice());
        viewHolderClass.product_name.setText(""+ mMenuDataModel.get(position).getProduct_name());
        viewHolderClass.product_desc.setText(""+ mMenuDataModel.get(position).getDesc());

        viewHolderClass.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, MenuDetailActivity.class);
                intent.putExtra("product_id",mMenuDataModel.get(position).getProduct_id());
                intent.putExtra("product_price", CommonUtilities.getFormatedDoubleValue(mMenuDataModel.get(position).getPrice()));
                intent.putExtra("product_name",mMenuDataModel.get(position).getProduct_name());
                intent.putExtra("product_desc",mMenuDataModel.get(position).getDesc());
                intent.putExtra("product_url",mMenuDataModel.get(position).getProduct_url());
                mContext.startActivity(intent);


            }
        });




    }


    public void clearData() {
        this.mMenuDataModel = null;
        this.mMenuDataModel = new ArrayList<MenuDataModel>();
    }
}
