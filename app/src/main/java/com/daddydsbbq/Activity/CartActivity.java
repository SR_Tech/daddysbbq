package com.daddydsbbq.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.daddydsbbq.Adapters.CartAdapter;
import com.daddydsbbq.ApiUtilities.RetrofitClient;
import com.daddydsbbq.Model.ResponseModel.CartModel;
import com.daddydsbbq.Others.AppUtilities;
import com.daddydsbbq.Others.CommonUtilities;
import com.daddydsbbq.Others.Messages;
import com.daddydsbbq.Others.SPUtilities;
import com.daddydsbbq.R;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CartActivity extends AppCompatActivity {
    String TAG = CartActivity.class.getName();
    Context mContext = CartActivity.this;
    public Dialog loadingDialog;

    ArrayList<CartModel> mCartModel;
    CartAdapter mCartAdapter;
    RecyclerView rvCartMenu;
    Button checkout_btn;
    double total_cart_value;
    public  android.app.AlertDialog alertDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);
        mCartModel = new ArrayList<>();
        rvCartMenu = findViewById(R.id._rv_cart_menu_item);
        rvCartMenu.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));

        checkout_btn = findViewById(R.id.cart_checkout_btn);
        if (CommonUtilities.isOnline(mContext)) {
            getCartItems();
        } else {
            CommonUtilities.showCommonTextAlert(mContext, Messages.INTERNET_ALERT);
        }
        checkout_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e(TAG, "onClick: total_cost " + total_cart_value);
                Intent intent = new Intent(mContext, AddressActivity.class);
                intent.putExtra("sub_total", "" + total_cart_value);
                startActivity(intent);
            }
        });

        findViewById(R.id.cart_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


    }
        //get cart items
        private void  getCartItems() {
            loadingDialog = ProgressDialog.show(mContext, "Please wait", "Loading...");
            String token = "Token "+ SPUtilities.getToken(mContext);
            Log.e(TAG, "Token: "+token);

            Call<ResponseBody> call = RetrofitClient.getmInstance().getApi().getCartMenuItems(token, SPUtilities.getCartId(mContext), AppUtilities.RestaurantGlobalid);
            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                    loadingDialog.dismiss();
                    int responseCode = response.code();
                    Log.e(TAG, "ResponseCode: "+responseCode);

                    if(responseCode == 200)
                    {
                        try {
                            String s = response.body().string();
                            Log.e(TAG, "Response: "+s);
                            JSONObject jsonObject=new JSONObject(s);
                            JSONArray resultsArray = jsonObject.getJSONArray("results");

                            if(resultsArray.length() <= 0)
                            {

                                Log.e(TAG, "No cart item ");
                                CommonUtilities.showCommonTextAlert(mContext,"Add item into the cart");
                                checkout_btn.setVisibility(View.GONE);

                            }
                            else
                            {
                                checkout_btn.setVisibility(View.VISIBLE);
                                total_cart_value = jsonObject.getDouble("total_cost");
                                checkout_btn.setText("Checkout $"+total_cart_value);

                                Log.e(TAG, "Carrt: "+resultsArray);
                                for(int i=0;i<resultsArray.length();i++)
                                {
                                    JSONObject json=resultsArray.getJSONObject(i);
                                    CartModel  cartModel = new CartModel();
                                    cartModel.setCart_menu_id(json.getInt("cart_item_id"));
                                    cartModel.setCart_menu_name(json.getString("product_name"));
                                    cartModel.setCart_menu_url(json.getString("product_url"));
                                    cartModel.setCart_menu_price(json.getDouble("price"));
                                    cartModel.setCart_menu_quantity(json.getInt("quantity"));
                                    cartModel.setCart_prroduct_id(json.getInt("product_id"));
                                    cartModel.setCart_menu_sequence_id(json.getInt("sequence_id"));

                                    cartModel.setIngrredient_array(json.getJSONArray("ingredient"));
                                    mCartModel.add(cartModel);

                                }

                                setMenuAdapter();
                            }

                        }
                        catch (NullPointerException e){}
                        catch (IOException e) { e.printStackTrace(); }
                        catch (JSONException e) {e.printStackTrace();}

                    }
                    else if(responseCode == 401)
                    {
                        Log.e(TAG, "ResponseError: "+responseCode);
                        CommonUtilities.sessionExpiredAlert(mContext);

                    }
                    else if(responseCode == 500)
                    {
                        Log.e(TAG, "ResponseError: "+responseCode);

                        try {
                            String s = response.errorBody().string();
                            JSONObject jsonObject = new JSONObject(s);
                            Log.e("TAG", "ResponseError object: "+jsonObject);
                            String msg = jsonObject.getString("msg");
                            Log.e("TAG", "Msg: "+msg);
                            CommonUtilities.showCommonTextAlert(mContext, msg);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                    }
                    else  {

                        CommonUtilities.showCommonTextAlert(mContext, Messages.UNKNOWN_RESPONSE);

                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {

                    Log.e(TAG, "onFailure: "+t.getMessage());
                    loadingDialog.dismiss();
                }
            });
        }

        private void setMenuAdapter() {
            mCartAdapter = new CartAdapter(mCartModel, this);
            rvCartMenu.setAdapter(mCartAdapter);
            mCartAdapter.notifyDataSetChanged();
            // let your adapter know about the changes and reload view.


            mCartAdapter.setOnDeleteClickListner(new CartAdapter.ICartMenuDeleteClickListner() {
                @Override
                public void onCartItemDeleteClick(View view, int position) {

                    Log.e(TAG, "CartItemDeleteClick: " );
                    if(CommonUtilities.isOnline(mContext))
                    {
                        deleteItemAlert(mCartModel.get(position).getCart_prroduct_id(),mCartModel.get(position).getCart_menu_sequence_id());

                    }
                    else
                    {
                        CommonUtilities.showCommonTextAlert(mContext,Messages.INTERNET_ALERT);
                    }

                }
            });

            mCartAdapter.setOnAddClickListner(new CartAdapter.ICartMenuAddClickListner() {
                @Override
                public void onCartItemAddClick(View view, int position, TextView tv_quantity) {

                    if(CommonUtilities.isOnline(mContext))
                    {

                        int itemQuantity = mCartModel.get(position).getCart_menu_quantity()+1;
                        int seq_id = mCartModel.get(position).getCart_menu_sequence_id();
                        int cart_id = SPUtilities.getCartId(mContext);
                        int cart_item_id = mCartModel.get(position).getCart_menu_id();
                        int product_id = mCartModel.get(position).getCart_prroduct_id();

                        updateItemQuantity(itemQuantity,seq_id,cart_id,cart_item_id,product_id);
                    }
                    else
                    {
                        CommonUtilities.showCommonTextAlert(mContext,Messages.INTERNET_ALERT);
                    }
                }
            });

            mCartAdapter.setOnMinusClickListner(new CartAdapter.ICartMenuMinusClickListner() {
                @Override
                public void onCartItemMinusClick(View view, int position, TextView tv_quantity) {

                    if(CommonUtilities.isOnline(mContext))
                    {


                        int itemQuantity = mCartModel.get(position).getCart_menu_quantity();

                        if(itemQuantity>1) {
                            itemQuantity = mCartModel.get(position).getCart_menu_quantity()-1;
                            int seq_id = mCartModel.get(position).getCart_menu_sequence_id();
                            int cart_id = SPUtilities.getCartId(mContext);
                            int cart_item_id = mCartModel.get(position).getCart_menu_id();
                            int product_id = mCartModel.get(position).getCart_prroduct_id();

                            updateItemQuantity(itemQuantity, seq_id, cart_id, cart_item_id, product_id);
                        }

                    }
                    else
                    {
                        CommonUtilities.showCommonTextAlert(mContext,Messages.INTERNET_ALERT);
                    }
                }
            });

        }



    private void deleteItemAlert(final int product_id,final int sequence_id)
    {
        LayoutInflater li = LayoutInflater.from(mContext);
        final View promptsView = li.inflate(R.layout.common_alert_layout2, null);
        android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(mContext);
        // set prompts.xml to alertdialog builder
        alertDialogBuilder.setView(promptsView);
        final TextView titalText = promptsView
                .findViewById(R.id.alert_msg);
        titalText.setText("Are you sure you want to delete?");


        promptsView.findViewById(R.id.btn_ok).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.cancel();
                deleteMenuItem( product_id,sequence_id);

            }
        });

        promptsView.findViewById(R.id.btn_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.cancel();
            }
        });
        // create alert dialog
        alertDialog = alertDialogBuilder.create();
        alertDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        alertDialog.show();


    }


    // delete item
    private void deleteMenuItem(int product_id, int sequence_id)
    {

        loadingDialog = ProgressDialog.show(mContext, "Please wait", "Loading...");

        String token = "Token "+ SPUtilities.getToken(mContext);
        Log.e(TAG, "Token: "+token);

        Call<ResponseBody> call = RetrofitClient.getmInstance().getApi().deletCartMenuItem(token, SPUtilities.getCartId(mContext),product_id,sequence_id);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                loadingDialog.dismiss();
                int responseCode = response.code();
                Log.e(TAG, "ResponseCode: "+responseCode);

                if(responseCode == 200)
                {
                    if(!mCartModel.isEmpty())
                    {
                        mCartAdapter.clearData();
                        mCartModel.clear();
                        mCartAdapter.notifyDataSetChanged();

                    }
                    getCartItems();
                }
                else if(responseCode == 401)
                {
                    Log.e(TAG, "ResponseError: "+responseCode);
                    CommonUtilities.sessionExpiredAlert(mContext);

                }
                else if(responseCode == 500)
                {
                    Log.e(TAG, "ResponseError: "+responseCode);

                    try {
                        String s = response.errorBody().string();
                        JSONObject jsonObject = new JSONObject(s);
                        Log.e("TAG", "ResponseError object: "+jsonObject);
                        String msg = jsonObject.getString("msg");
                        Log.e("TAG", "Msg: "+msg);
                        CommonUtilities.showCommonTextAlert(mContext, msg);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
                else  {

                    CommonUtilities.showCommonTextAlert(mContext, Messages.UNKNOWN_RESPONSE);

                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                Log.e(TAG, "onFailure: "+t.getMessage());
                loadingDialog.dismiss();
            }
        });
    }


    private void updateItemQuantity(final int itemQuantity,final int seq_id,final int cart_id,final int cart_item_id,final int product_id)
    {

        loadingDialog = ProgressDialog.show(mContext, "Please wait", "Loading...");

        JsonObject dataObject = new JsonObject();
        dataObject.addProperty("id", cart_item_id);
        dataObject.addProperty("product_id", product_id);
        dataObject.addProperty("sequence_id", seq_id);
        dataObject.addProperty("cart", cart_id);
        dataObject.addProperty("quantity", itemQuantity);
        dataObject.addProperty("ingredient_id", 0);


        String token = "Token "+ SPUtilities.getToken(mContext);
        Log.e(TAG, "Token: "+token);

        Call<ResponseBody> call = RetrofitClient.getmInstance().getApi().updateCartMenuItem(token,cart_item_id,dataObject);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                loadingDialog.dismiss();
                int responseCode = response.code();
                Log.e(TAG, "ResponseCode: "+responseCode);

                if(responseCode == 200)
                {
                    if(!mCartModel.isEmpty())
                    {
                        mCartAdapter.clearData();
                        mCartModel.clear();
                        mCartAdapter.notifyDataSetChanged();

                    }
                    getCartItems();
                }
                else if(responseCode == 401)
                {
                    Log.e(TAG, "ResponseError: "+responseCode);
                    CommonUtilities.sessionExpiredAlert(mContext);

                }
                else if(responseCode == 500)
                {
                    Log.e(TAG, "ResponseError: "+responseCode);

                    try {
                        String s = response.errorBody().string();
                        JSONObject jsonObject = new JSONObject(s);
                        Log.e("TAG", "ResponseError object: "+jsonObject);
                        String msg = jsonObject.getString("msg");
                        Log.e("TAG", "Msg: "+msg);
                        CommonUtilities.showCommonTextAlert(mContext, msg);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
                else  {

                    CommonUtilities.showCommonTextAlert(mContext, Messages.UNKNOWN_RESPONSE);

                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                Log.e(TAG, "onFailure: "+t.getMessage());
                loadingDialog.dismiss();
            }
        });
    }
    }
