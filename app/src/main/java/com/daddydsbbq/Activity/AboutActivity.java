package com.daddydsbbq.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.daddydsbbq.ApiUtilities.RetrofitClient;
import com.daddydsbbq.Others.AppUtilities;
import com.daddydsbbq.Others.CommonUtilities;
import com.daddydsbbq.Others.Messages;
import com.daddydsbbq.Others.SPUtilities;
import com.daddydsbbq.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AboutActivity extends AppCompatActivity {
    String TAG = AboutActivity.class.getName();
    ImageView about_img;
    TextView about_text;
    Context mContext = AboutActivity.this;
    private Dialog loadingDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        about_img = findViewById(R.id.about_image);
        about_text = findViewById(R.id.about_text);
        findViewById(R.id.about_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        if (CommonUtilities.isOnline(mContext)) {

            getAboutDetail();
        } else {
            CommonUtilities.showCommonTextAlert(mContext, Messages.INTERNET_ALERT);
        }
    }

    private void getAboutDetail() {

            loadingDialog = ProgressDialog.show(mContext, "Please wait", "Loading...");

            String token = SPUtilities.getToken(mContext);
            Log.e(TAG, "Token: " + token);

            int custId = SPUtilities.getCustomerId(mContext);
            Call<ResponseBody> call = RetrofitClient.getmInstance().getApi().getAboutRestaurant("Token " + token, AppUtilities.RestaurantGlobalid);
            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                    loadingDialog.dismiss();
                    int responseCode = response.code();
                    Log.e(TAG, "ResponseCode: " + responseCode);

                    if (responseCode == 200) {
                        try {
                            String s = response.body().string();
                            Log.e(TAG, "Response: " + s);
                            JSONObject jsonObject = new JSONObject(s);
                            about_text.setText(jsonObject.getString("desc"));
                            String img_url = jsonObject.getString("restaurant_url");

                            Glide.with(mContext)
                                    .load(img_url)
//                .placeholder(R.drawable.myplaceholder)
                                    .into(about_img);



                        } catch (NullPointerException e) {
                        } catch (IOException e) {
                            e.printStackTrace();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    } else if (responseCode == 401) {
                        Log.e(TAG, "ResponseError: " + responseCode);
                        CommonUtilities.sessionExpiredAlert(mContext);

                    } else if (responseCode == 500) {
                        Log.e(TAG, "ResponseError: " + responseCode);

                        try {
                            String s = response.errorBody().string();
                            JSONObject jsonObject = new JSONObject(s);
                            Log.e("TAG", "ResponseError object: " + jsonObject);
                            String msg = jsonObject.getString("msg");
                            Log.e("TAG", "Msg: " + msg);
                            CommonUtilities.showCommonTextAlert(mContext, msg);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                    } else {

                        CommonUtilities.showCommonTextAlert(mContext, Messages.UNKNOWN_RESPONSE);

                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {

                    Log.e(TAG, "onFailure: " + t.getMessage());
                    loadingDialog.dismiss();
                }
            });
        }


}