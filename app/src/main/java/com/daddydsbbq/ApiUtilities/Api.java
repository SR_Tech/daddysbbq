package com.daddydsbbq.ApiUtilities;

import com.daddydsbbq.Model.RequestModel.AddBillingAddress;
import com.daddydsbbq.Model.RequestModel.AddUserModel;
import com.daddydsbbq.Model.RequestModel.ChangePasswordModel;
import com.daddydsbbq.Model.RequestModel.ForgotPasswordModel;
import com.daddydsbbq.Model.RequestModel.ForgotUsernameModel;
import com.daddydsbbq.Model.RequestModel.GetOTPModel;
import com.daddydsbbq.Model.RequestModel.GetUserRegisrationIdModel;
import com.daddydsbbq.Model.RequestModel.LoginModel;
import com.daddydsbbq.Model.RequestModel.OTPVerificationModel;
import com.daddydsbbq.Model.RequestModel.UpdateProfileModel;
import com.daddydsbbq.Model.RequestModel.UserRegistrationModel;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface Api {

//
    //1
    @Headers("Content-Type: application/json")
    @POST("rest-auth/login/v1/")
    Call<ResponseBody> login(
            @Body LoginModel login
    );
//
    //2
    @Headers("Content-Type: application/json")
    @GET("user/")
    Call<ResponseBody> getUserIid(
            @Header("Authorization") String authorization,
            @Query("username") String username
    );
//
    //3
    @Headers("Content-Type: application/json")
    @POST("userrestaurant/")
    Call<ResponseBody> addUserToRestaurrant(
            @Header("Authorization") String authorization,
            @Body AddUserModel addUserModel
    );

   //4
    @Headers("Content-Type: application/json")
    @GET("cart/?")
    Call<ResponseBody> checkCartIsCreated(
            @Header("Authorization") String token,
            @Query("customer_id") String custid,
            @Query("restaurant") String restId
    );


    //5
    @FormUrlEncoded
    @POST("cart/")
    Call<ResponseBody> createCart(
            @Header("Authorization") String token,
            @Field("customer_id") String custid,
            @Field("restaurant") String restId
    );

    //6
    @POST("graphql/")
    Call<ResponseBody> getCartCount(
            @Body RequestBody query
    );
//
//    //7
    @Headers("Content-Type: application/json")
    @POST("user/")
    Call<ResponseBody> getUserRegId(
            @Header("Authorization") String token,
            @Body GetUserRegisrationIdModel mUserRegisrationModel
    );
//
    //8
    @Headers("Content-Type: application/json")
    @POST("customer/")
    Call<ResponseBody> userRegistration(
            @Header("Authorization") String token,
            @Body UserRegistrationModel mUserRegistrationMoodel
    );

    //9
    @Headers("Content-Type: application/json")
    @POST("forgot/user/")
    Call<ResponseBody> forgotUsername(
            @Header("Authorization") String token,
            @Body ForgotUsernameModel mForgotUsernameModel
    );
//
    //10
    @Headers("Content-Type: application/json")
    @POST("rest-auth/password/reset/v1/")
    Call<ResponseBody> forgotPassword(
            @Header("Authorization") String token,
            @Body ForgotPasswordModel mForgotPasswordModel);
//
    //11
    @Headers("Content-Type: application/json")
    @GET("customer/?")
    Call<ResponseBody> getProfile(
            @Header("Authorization") String authorization,
            @Query("customer_id") String customer_id
    );
//
    //12
    @Headers("Content-Type: application/json")
    @PUT("user/{id}/")
    Call<ResponseBody> updateProfile(
            @Header("Authorization") String authorization,
            @Path("id") String id,
            @Body UpdateProfileModel mUpdateProfileModel);
//
    //13
    @Headers("Content-Type: application/json")
    @PUT("customer/{id}/")
    Call<ResponseBody> updateCustomerMobile(
            @Header("Authorization") String authorization,
            @Path("id") String id,
            @Body UpdateProfileModel mUpdateProfileModel);
//
    //14
    @POST("rest-auth/password/reset/confirm/v1/")
    Call<ResponseBody> changePassword(
            @Header("Authorization") String authorization,
            @Body ChangePasswordModel mChangePasswordModel

    );
//
    //15
    @POST("send/code/")
    Call<ResponseBody> getOTP(
            @Body GetOTPModel mobileVerification

    );
//
    //16
    @POST("guest/verify/")
    Call<ResponseBody> verifyOTP(
            @Body OTPVerificationModel otpVerificationModel

    );
//
//    //17
//    @Headers("Content-Type: application/json")
//    @GET("catalog/?")
//    Call<ResponseBody> getCategoryMenu(
//            @Header("Authorization") String token,
//            @Query("restaurant_id") String id,
//            @Query("category") String category,
//            @Query("status") String status
//
////            @Header("user_id") String custidStr,
////            @Header("action") String cart
//
//    );
//
//    //18
//    @Headers("Content-Type: application/json")
//    @GET("hour/?")
//    Call<ResponseBody> getTiming(
//            @Header("Authorization") String token,
//            @Query("restaurant_id") String id
//    );
//
    //19
    @Headers("Content-Type: application/json")
    @GET("category/?")
    Call<ResponseBody> getCategory(
            @Header("Authorization") String token,
            @Query("restaurant_id") String id
    );
//
//
//    //20
    @Headers("Content-Type: application/json")
    @GET("catalog/?")
    Call<ResponseBody> getCategoryMenuList(
            @Header("Authorization") String token,
            @Query("restaurant_id") String id,
            @Query("category_id") int category_id,
            @Query("status") String status,
            @Query("page") int pagecount

//            @Header("user_id") String custidStr,
//            @Header("action") String cart

    );
//
//    //21
    @GET("restaurant/{id}/")
    Call<ResponseBody> getAboutRestaurant(
            @Header("Authorization") String authorization,
            @Path("id") String id

    );
//
//
    //22
    @POST("cart-item/")
    Call<ResponseBody> addMenuToCart(
            @Header("Authorization") String authorization,
            @Body JsonArray login
    );
//
//    //23
//    @Headers("Content-Type: application/json")
//    @GET("tax/?country_code=US")
//    Call<ResponseBody> getStateList(
//      @Header("Authorization") String authorization
//    );
//
//    //24
//    @Headers("Content-Type: application/json")
//    @GET("billing/")
//    Call<ResponseBody> getBillingAddress(
//            @Header("Authorization") String authorization,
//            @Query("customer_id") int cust_id
//    );
//
    //25deletCartMenuItem
    @Headers("Content-Type: application/json")
    @POST("billing/")
    Call<ResponseBody> addBillingAddress(
            @Header("Authorization") String authorization,
            @Body AddBillingAddress addBillingAddress

    );
//
//    //26
//    @Headers("Content-Type: application/json")
//    @PUT("billing/{id}/")
//    Call<ResponseBody> updateBillingAddress(
//            @Header("Authorization") String authorization,
//            @Path("id") int id,
//            @Body AddBillingAddress addBillingAddress
//    );
//
//
//    //27
//    @Headers("Content-Type: application/json")
//    @GET("ingredient/?status=ACTIVE")
//    Call<ResponseBody> getMenuIngredient(
//            @Header("Authorization") String authorization,
//            @Query("product_id") int product_id
//    );
//
    //28
    @Headers("Content-Type: application/json")
    @GET("cart-item/?status=ACTIVE")
    Call<ResponseBody> getCartMenuItems(
            @Header("Authorization") String authorization,
            @Query("cart_id") int cart_id,
            @Query("restaurant_id") String retroid
    );
//
//
//    //29
    @Headers("Content-Type: application/json")
    @DELETE("cart-item/{id}/")
    Call<ResponseBody> deletCartMenuItem(
            @Header("Authorization") String authorization,
            @Path("id") int rid,
            @Query("product_id") int sid,
            @Query("sequence_id") int sequence_id
    );
//
    //30

    @PUT("cart-item/{id}/")
    Call<ResponseBody> updateCartMenuItem(
            @Header("Authorization") String authorization,
            @Path("id") int id,
            @Body JsonObject cartUpdate
    );
//
//    //31
//    @GET("shipping-method/?status=ACTIVE")
//    Call<ResponseBody> getShippingMethod(
//            @Header("Authorization") String authorization,
//            @Query("restaurant") String rid
//    );
//
//    //32
//    @POST("fee/")
//    Call<ResponseBody> getTaxFee(
//            @Header("Authorization") String authorization,
//            @Body GetTaxFee query
//    );
//
////    @POST("fee/")
////    Call<ResponseBody> updatefree(
////            @Header("Authorization") String authorization,
////            @Body UpdateFree query
////    );
//
//    //33
//    @POST("order-detail/")
//    Call<ResponseBody> submitOrderDetail(
//            @Header("Authorization") String authorization,
//            @Body SubmitOrderDetails orderdetails
//    );
//
//    //34
//    @POST("payment/")
//    Call<ResponseBody> payment(
//            @Header("Authorization") String authorization,
//            @Body JsonObject orderdetail
//    );
//
//    //35
//    @GET("customer-payment/")
//    Call<ResponseBody> getOrderList(
//            @Header("Authorization") String authorization,
//            @Query("restaurant_id") String restaurant_id,
//            @Query("customer_id") int customer_id
//    );
//
//    //36
//    @GET("restaurant/?group_name=madurai")
//    Call<ResponseBody> getLocation(
//            @Header("Authorization") String authorization
//    );
//
//    //37
//    @GET("order-item/")
//    Call<ResponseBody> getOrderItemList(
//            @Header("Authorization") String authorization,
//            @Query("order_id") int orderid
//    );

}
