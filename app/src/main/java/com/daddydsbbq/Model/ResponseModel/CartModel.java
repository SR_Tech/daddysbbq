package com.daddydsbbq.Model.ResponseModel;

import org.json.JSONArray;

public class CartModel {


    //private JSONObject jsonObject;
    private int cart_menu_id;
    private int cart_prroduct_id;
    private int cart_menu_sequence_id;


    private String cart_menu_name;
    private double cart_menu_price;
    private int cart_menu_quantity;
    private String cart_menu_url;

    private JSONArray ingrredient_array;

    public int getCart_menu_id() {
        return cart_menu_id;
    }

    public void setCart_menu_id(int cart_menu_id) {
        this.cart_menu_id = cart_menu_id;
    }

    public String getCart_menu_name() {
        return cart_menu_name;
    }

    public void setCart_menu_name(String cart_menu_name) {
        this.cart_menu_name = cart_menu_name;
    }

    public double getCart_menu_price() {
        return cart_menu_price;
    }

    public void setCart_menu_price(double cart_menu_price) {
        this.cart_menu_price = cart_menu_price;
    }

    public int getCart_prroduct_id() {
        return cart_prroduct_id;
    }

    public void setCart_prroduct_id(int cart_prroduct_id) {
        this.cart_prroduct_id = cart_prroduct_id;
    }

    public int getCart_menu_sequence_id() {
        return cart_menu_sequence_id;
    }

    public void setCart_menu_sequence_id(int cart_menu_sequence_id) {
        this.cart_menu_sequence_id = cart_menu_sequence_id;
    }

    public int getCart_menu_quantity() {
        return cart_menu_quantity;
    }

    public void setCart_menu_quantity(int cart_menu_quantity) {
        this.cart_menu_quantity = cart_menu_quantity;

    }

    public String getCart_menu_url() {
        return cart_menu_url;
    }

    public void setCart_menu_url(String cart_menu_url) {
        this.cart_menu_url = cart_menu_url;
    }

    public JSONArray getIngrredient_array() {
        return ingrredient_array;
    }

    public void setIngrredient_array(JSONArray ingrredient_array) {
        this.ingrredient_array = ingrredient_array;
    }


}
