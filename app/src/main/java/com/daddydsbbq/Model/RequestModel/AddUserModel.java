package com.daddydsbbq.Model.RequestModel;

public class AddUserModel {
    final int user;
    final String restaurant;


    public AddUserModel(int user, String restaurant) {
        this.user = user;
        this.restaurant = restaurant;
    }
}