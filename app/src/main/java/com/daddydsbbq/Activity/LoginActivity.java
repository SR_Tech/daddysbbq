package com.daddydsbbq.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.daddydsbbq.ApiUtilities.RetrofitClient;
import com.daddydsbbq.Model.RequestModel.AddUserModel;
import com.daddydsbbq.Model.RequestModel.ForgotPasswordModel;
import com.daddydsbbq.Model.RequestModel.ForgotUsernameModel;
import com.daddydsbbq.Model.RequestModel.LoginModel;
import com.daddydsbbq.Others.AppUtilities;
import com.daddydsbbq.Others.CommonUtilities;
import com.daddydsbbq.Others.Messages;
import com.daddydsbbq.Others.SPUtilities;
import com.daddydsbbq.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {


    EditText mUsernameEditext,mPasswordEdittext;
    String username, password;
    Button buttonLogin;
    TextView buttonSignup;
    Context mContext = LoginActivity.this;
    public Dialog loadingDialog ;
    TextView tvForgotUsername, tvForgotPassword;

    AlertDialog alertDialog;  // promptview
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mUsernameEditext = findViewById(R.id.login_username);
        mPasswordEdittext = findViewById(R.id.login_password);
        buttonLogin = findViewById(R.id.logibButton);
        buttonSignup = findViewById(R.id.signup_text);
        tvForgotPassword = findViewById(R.id.login_forgotpassword);
        tvForgotUsername = findViewById(R.id.logiin_forgotusername);
        findViewById(R.id.login_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.e("TAG", "LOGIN BUTTON CLICKED ");
                username = mUsernameEditext.getText().toString().trim();
                password = mPasswordEdittext.getText().toString().trim();

                if(username.isEmpty())
                {
                    CommonUtilities.showCommonTextAlert(mContext,"Enter username");
                }else if(password.isEmpty())
                {
                    CommonUtilities.showCommonTextAlert(mContext,"Enter password");

                }
                else {

                    if (CommonUtilities.isOnline(mContext)) {
                        loginAPi();

                    } else {
                        CommonUtilities.showCommonTextAlert(mContext, Messages.INTERNET_ALERT);
                    }
                }
            }
        });
        buttonSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, SignUpActivity.class);
                startActivity(intent);
            }
        });

        findViewById(R.id.guestUserButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, GuestMobileVerification.class);
                startActivity(intent);
            }
        });
        tvForgotUsername.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                LayoutInflater li = LayoutInflater.from(mContext);
                final View promptsView = li.inflate(R.layout.dialog_forgot, null);
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mContext);
                // set prompts.xml to alertdialog builder
                alertDialogBuilder.setView(promptsView);
                final TextView titalText =  promptsView
                        .findViewById(R.id.resetText);

                titalText.setText("Reset username");
                final EditText inputText =  promptsView
                        .findViewById(R.id.dialog_input);
                inputText.setHint("Enter your email");
                promptsView.findViewById(R.id.btn_ok).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        if(inputText.getText().toString().trim().isEmpty())
                        {
                            inputText.setError("Enter your email");
                        }
                        else if(!CommonUtilities.isValidEmail(inputText.getText().toString().trim()))
                        {
                            inputText.setError("Enter valid email");
                            inputText.setFocusable(true);

                        }
                        else
                        {

                            if(CommonUtilities.isOnline(mContext))
                            {
                                alertDialog.cancel();
                                forgot_username(inputText.getText().toString().trim());                                // call forgot username api
                            }
                            else {
                                CommonUtilities.showCommonTextAlert(mContext, Messages.INTERNET_ALERT);


                            }
                        }
                    }
                });

                promptsView.findViewById(R.id.btn_cancel).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        alertDialog.cancel();

                    }
                });
                // create alert dialog
                alertDialog = alertDialogBuilder.create();

                // show it
                alertDialog.show();
            }
        });
        tvForgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                LayoutInflater li = LayoutInflater.from(mContext);
                final View promptsView = li.inflate(R.layout.dialog_forgot, null);
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mContext);
                // set prompts.xml to alertdialog builder
                alertDialogBuilder.setView(promptsView);
                final TextView titalText =  promptsView
                        .findViewById(R.id.resetText);

                titalText.setText("Reset password");
                final EditText inputText =  promptsView
                        .findViewById(R.id.dialog_input);
                inputText.setHint("Enter your username");
                promptsView.findViewById(R.id.btn_ok).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        if(inputText.getText().toString().trim().isEmpty())
                        {
                            inputText.setError("Enter your username");
                        }
                        else
                        {

                            if(CommonUtilities.isOnline(mContext))
                            {
                                alertDialog.cancel();
                                forgotPassword(inputText.getText().toString().trim());// call forgot password api
                            }
                            else {
                                CommonUtilities.showCommonTextAlert(mContext, Messages.INTERNET_ALERT);


                            }
                        }
                    }
                });

                promptsView.findViewById(R.id.btn_cancel).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        alertDialog.cancel();

                    }
                });
                // create alert dialog
                alertDialog = alertDialogBuilder.create();
                // show it
                alertDialog.show();


            }
        });

}


    //login api call
    void loginAPi()
    {

        loadingDialog = ProgressDialog.show(mContext, "Please wait", "Loading...");

        Call<ResponseBody> call = RetrofitClient.getmInstance().getApi().login(new LoginModel(username,password, AppUtilities.RestaurantGlobalid));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                loadingDialog.dismiss();


                int responseCode = response.code();
                Log.e("TAG", "ResponseCode: "+responseCode);

                if(responseCode == 200)
                {

                    try {
                        String s = response.body().string();
                        Log.e("TAG", "Response: "+s);
                        JSONObject jsonObject=new JSONObject(s);
                        String token = jsonObject.getString("token");
                        int id = jsonObject.getInt("id");

                        Log.e("TAG", "Id: "+id);
                        Log.e("Loginctivity", "Token: "+token);

                        SPUtilities.setToken(mContext,token);
                        SPUtilities.setCustomerId(mContext,id);
                        SPUtilities.setLoginStatus(mContext,true);
                        SPUtilities.setUserType(mContext,"REGISTERED");



                        Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
                        startActivity(intent);

                    }
                    catch (NullPointerException e){}
                    catch (IOException e) { e.printStackTrace(); }
                    catch (JSONException e) {e.printStackTrace();}

                }
                else if(responseCode == 401)
                {
                    Log.e("TAG", "ResponseError: "+responseCode);

                    CommonUtilities.showCommonTextAlert(LoginActivity.this, "Username or password is incorrect");

                }
                else if(responseCode == 403)
                {
                    try {

                        String s = response.errorBody().string();
                        JSONObject jsonObject = new JSONObject(s);
                        Log.e("TAG", "ResponseError object: "+jsonObject);
                        String msg = jsonObject.getString("msg");
                        Log.e("TAG", "Msg: "+msg);
                        if (msg.equals("You do not have access to restaurant.")){


                            LayoutInflater li = LayoutInflater.from(mContext);
                            final View promptsView = li.inflate(R.layout.common_alert_layout2, null);
                            android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(mContext);
                            // set prompts.xml to alertdialog builder
                            alertDialogBuilder.setView(promptsView);
                            final TextView titalText = promptsView
                                    .findViewById(R.id.alert_msg);
                            titalText.setText("You do not have access to this restaurant. You want to login for this restaurant please click on OK.");


                            promptsView.findViewById(R.id.btn_ok).setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    alertDialog.cancel();

                                    if( CommonUtilities.isOnline(mContext)) {

                                        tokenApi();
                                    }
                                    else
                                    {
                                        CommonUtilities.showCommonTextAlert(mContext,Messages.INTERNET_ALERT);
                                    }


                                }
                            });

                            promptsView.findViewById(R.id.btn_cancel).setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    alertDialog.cancel();
                                }
                            });
                            // create alert dialog
                            alertDialog = alertDialogBuilder.create();
                            alertDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                            alertDialog.show();


                        }
                        else {
                            CommonUtilities.showCommonTextAlert(mContext, "The system is temporarily unavailable. Please try again later");

                        }
                    }
                    catch (NullPointerException e){}
                    catch (JSONException e) {e.printStackTrace();}
                    catch (IOException e) {
                        e.printStackTrace();
                    }

                }
                else  {

                    CommonUtilities.showCommonTextAlert(mContext, "The system is temporarily unavailable. Please try again later");

                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                Log.e("TAG", "onFailure: "+t.getMessage());
                loadingDialog.dismiss();
            }
        });
    }
    //login api call
    void tokenApi()
    {

        loadingDialog = ProgressDialog.show(mContext, "Please wait", "Loading...");

        Call<ResponseBody> call = RetrofitClient.getmInstance().getApi().login(new LoginModel(username,password, AppUtilities.RESTAURANT_ID));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                loadingDialog.dismiss();
                int responseCode = response.code();
                Log.e("TAG", "ResponseCode: "+responseCode);

                if(responseCode == 200)
                {
                    try {
                        String s = response.body().string();
                        Log.e("TAG", "Response: "+s);
                        JSONObject jsonObject=new JSONObject(s);
                        String token = jsonObject.getString("token");

                        Log.e("TAG", "Token: "+token);
                        SPUtilities.setToken(mContext,token);

                        getUserId(token);


                    }
                    catch (NullPointerException e){}
                    catch (IOException e) { e.printStackTrace(); }
                    catch (JSONException e) {e.printStackTrace();}

                }
                else if(responseCode == 401)
                {
                    Log.e("TAG", "ResponseError: "+responseCode);

                }
                else if(responseCode == 500)
                {
                    Log.e("TAG", "ResponseError: "+responseCode);
                    try {
                        String s = response.body().string();
                        Log.e("TAG", "Response: "+s);
                        JSONObject jsonObject=new JSONObject(s);
                        String msg = jsonObject.getString("msg");
                        Log.e("TAG", "Msg: "+msg);

                        CommonUtilities.showCommonTextAlert(mContext, msg);

                    }
                    catch (NullPointerException e){}
                    catch (IOException e) { e.printStackTrace(); }
                    catch (JSONException e) {e.printStackTrace();}

                }
                else  {

                    CommonUtilities.showCommonTextAlert(mContext, "The system is temporarily unavailable. Please try again later");

                }


            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                Log.e("TAG", "onFailure: "+t.getMessage());
                loadingDialog.dismiss();
            }
        });
    }

    private void getUserId(final String token)
    {
        loadingDialog = ProgressDialog.show(mContext, "Please wait", "Loading...");
        Call<ResponseBody> call = RetrofitClient.getmInstance().getApi().getUserIid("Token "+token,username);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                loadingDialog.dismiss();


                int responseCode = response.code();
                Log.e("TAG", "ResponseCode: "+responseCode);

                if(responseCode == 200)
                {

                    try {
                        String s = response.body().string();
                        Log.e("TAG", "Response: "+s);
                        JSONObject jsonObject=new JSONObject(s);
                        JSONArray jsonArray = jsonObject.getJSONArray("results");
                        JSONObject dataObject = jsonArray.getJSONObject(0);
                        int userid = dataObject.getInt("id");

                        addUserToRestaurant(userid, token);

                    }
                    catch (NullPointerException e){}
                    catch (IOException e) { e.printStackTrace(); }
                    catch (JSONException e) {e.printStackTrace();}

                }
                else if(responseCode == 400 || responseCode == 500)
                {
                    Log.e("TAG", "ResponseError: "+responseCode);
                    try {
                        String s = response.body().string();
                        Log.e("TAG", "Response: "+s);
                        JSONObject jsonObject=new JSONObject(s);
                        String msg = jsonObject.getString("msg");
                        Log.e("TAG", "Msg: "+msg);

                        CommonUtilities.showCommonTextAlert(mContext, msg);
                    }
                    catch (NullPointerException e){}
                    catch (IOException e) { e.printStackTrace(); }
                    catch (JSONException e) {e.printStackTrace();}

                }
                else  {

                    CommonUtilities.showCommonTextAlert(mContext, "The system is temporarily unavailable. Please try again later");

                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                Log.e("TAG", "onFailure: "+t.getMessage());
                loadingDialog.dismiss();
            }
        });
    }

    //addUserToRestaurant
    private void addUserToRestaurant(int userid, String token)
    {
        loadingDialog = ProgressDialog.show(mContext, "Please wait", "Loading...");
        Call<ResponseBody> call = RetrofitClient.getmInstance().getApi().addUserToRestaurrant("Token "+token,new AddUserModel(userid,AppUtilities.RestaurantGlobalid));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                loadingDialog.dismiss();


                int responseCode = response.code();
                Log.e("TAG", "ResponseCode: "+responseCode);

                if(responseCode == 201)
                {
                    loginAPi();
                }
                else if(responseCode == 400 || responseCode == 500)
                {
                    Log.e("TAG", "ResponseError: "+responseCode);
                    try {
                        String s = response.body().string();
                        Log.e("TAG", "Response: "+s);
                        JSONObject jsonObject=new JSONObject(s);
                        String msg = jsonObject.getString("msg");
                        Log.e("TAG", "Msg: "+msg);

                        CommonUtilities.showCommonTextAlert(mContext, msg);
                    }
                    catch (NullPointerException e){}
                    catch (IOException e) { e.printStackTrace(); }
                    catch (JSONException e) {e.printStackTrace();}

                }
                else  {

                    CommonUtilities.showCommonTextAlert(mContext, "The system is temporarily unavailable. Please try again later");

                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                Log.e("TAG", "onFailure: "+t.getMessage());
                loadingDialog.dismiss();
            }
        });
    }

    private void forgot_username(String email)
    {
        loadingDialog = ProgressDialog.show(mContext, "Please wait", "Loading...");

        String token = SPUtilities.getToken(mContext);

        Call<ResponseBody> call = RetrofitClient.getmInstance().getApi().forgotUsername("Token "+token,new ForgotUsernameModel(email));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                loadingDialog.dismiss();


                int responseCode = response.code();
                Log.e("TAG", "ResponseCode: "+responseCode);

                if(responseCode == 200)
                {

                    try {
                        String s = response.body().string();
                        Log.e("TAG", "Response: "+s);
                        JSONObject jsonObject=new JSONObject(s);
                        CommonUtilities.showCommonTextAlert(mContext,jsonObject.getString("msg"));
                    }
                    catch (NullPointerException e){}
                    catch (IOException e) { e.printStackTrace(); }
                    catch (JSONException e) {e.printStackTrace();}
                }
                else if(responseCode == 400 || responseCode == 500)
                {
                    Log.e("TAG", "ResponseError: "+responseCode);
                    try {
                        String s = response.body().string();
                        Log.e("TAG", "Response: "+s);
                        JSONObject jsonObject=new JSONObject(s);
                        String msg = jsonObject.getString("msg");
                        Log.e("TAG", "Msg: "+msg);

                        CommonUtilities.showCommonTextAlert(mContext, msg);
                    }
                    catch (NullPointerException e){}
                    catch (IOException e) { e.printStackTrace(); }
                    catch (JSONException e) {e.printStackTrace();}
                }
                else  {
                    CommonUtilities.showCommonTextAlert(mContext, "The system is temporarily unavailable. Please try again later");
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                Log.e("TAG", "onFailure: "+t.getMessage());
                loadingDialog.dismiss();
            }
        });
    }

    // forgot password
    private void forgotPassword(String username)
    {
        {
            loadingDialog = ProgressDialog.show(mContext, "Please wait", "Loading...");

            String token = SPUtilities.getToken(mContext);

            Call<ResponseBody> call = RetrofitClient.getmInstance().getApi().forgotPassword("Token "+token,new ForgotPasswordModel(username));
            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                    loadingDialog.dismiss();


                    int responseCode = response.code();
                    Log.e("TAG", "ResponseCode: "+responseCode);

                    if(responseCode == 200)
                    {

                        try {
                            String s = response.body().string();
                            Log.e("TAG", "Response: "+s);
                            JSONObject jsonObject=new JSONObject(s);
                            CommonUtilities.showCommonTextAlert(mContext,Messages.FORGOT_PASSWORD);
                        }
                        catch (NullPointerException e){}
                        catch (IOException e) { e.printStackTrace(); }
                        catch (JSONException e) {e.printStackTrace();}

                    }
                    else if(responseCode == 400 || responseCode == 500)
                    {
                        Log.e("TAG", "ResponseError: "+responseCode);
                        try {
                            String s = response.body().string();
                            Log.e("TAG", "Response: "+s);
                            JSONObject jsonObject=new JSONObject(s);
                            String msg = jsonObject.getString("msg");
                            Log.e("TAG", "Msg: "+msg);

                            CommonUtilities.showCommonTextAlert(mContext, msg);
                        }
                        catch (NullPointerException e){}
                        catch (IOException e) { e.printStackTrace(); }
                        catch (JSONException e) {e.printStackTrace();}

                    }
                    else  {

                        CommonUtilities.showCommonTextAlert(mContext, "The system is temporarily unavailable. Please try again later");

                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {

                    Log.e("TAG", "onFailure: "+t.getMessage());
                    loadingDialog.dismiss();
                }
            });
        }
    }
}