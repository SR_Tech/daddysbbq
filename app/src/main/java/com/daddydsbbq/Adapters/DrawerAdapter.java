package com.daddydsbbq.Adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.daddydsbbq.Model.ResponseModel.HomeMenuModel;
import com.daddydsbbq.R;

import java.util.ArrayList;

public class DrawerAdapter extends BaseAdapter {

    Context context;
    public String[] menuArray;


    LayoutInflater inflater;

    public DrawerAdapter(Context context, String[] menuArray) {
        this.context = context;
        this.menuArray = menuArray;
        Log.e("Adapter", "DrawerAdapter: "+menuArray );
    }

    @Override
    public int getCount() {
        return menuArray.length;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (inflater == null) {
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
        if (convertView == null){
            convertView = inflater.inflate(R.layout.drawer_menu_layout,null);
        }

        ImageView imageView = convertView.findViewById(R.id.drawer_menu_img);
        TextView textView = convertView.findViewById(R.id.drawer_menu_name);
        // imageView.setImageResource(MenuImage[position]);
        textView.setText(""+ menuArray[position]);
        return convertView;
    }
}
