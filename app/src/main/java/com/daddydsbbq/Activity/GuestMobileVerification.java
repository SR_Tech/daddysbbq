package com.daddydsbbq.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.daddydsbbq.ApiUtilities.RetrofitClient;
import com.daddydsbbq.Model.RequestModel.GetOTPModel;
import com.daddydsbbq.Model.RequestModel.OTPVerificationModel;
import com.daddydsbbq.Others.CommonUtilities;
import com.daddydsbbq.Others.Messages;
import com.daddydsbbq.Others.SPUtilities;
import com.daddydsbbq.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GuestMobileVerification extends AppCompatActivity {

    EditText etCode, etMblno;
    private Context mContext = GuestMobileVerification.this;
    private Dialog loadingDialog;
    String mobileno,otp;
    AlertDialog alertDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guest_mobile_verification);


        etCode = findViewById(R.id.edt_code);
        etMblno = findViewById(R.id.edt_m_mno);
        findViewById(R.id.guest_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        findViewById(R.id.guest_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        findViewById(R.id.guest_send_otp_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String mblno = etMblno.getText().toString().trim();
                String code = etCode.getText().toString().trim();

                if(mblno.isEmpty())
                {
                    CommonUtilities.showCommonTextAlert(mContext,"Enter mobile number");
                }else if(mblno.length() < 8)
                {
                    CommonUtilities.showCommonTextAlert(mContext,"Enter valid mobile number");
                }
                if(code.isEmpty())
                {
                    CommonUtilities.showCommonTextAlert(mContext,"Enter country code");
                }
                else
                {
                    mobileno = code+mblno;
                    getOTP();
                }

            }
        });
    }


    //get OTP
    private void getOTP()
    {
        loadingDialog = ProgressDialog.show(mContext, "Please wait", "Loading...");
        int custId = SPUtilities.getCustomerId(mContext);
        String token = SPUtilities.getToken(mContext);
        Log.e("ChangePassword", "Token: "+token);

        Call<ResponseBody> call = RetrofitClient.getmInstance().getApi().getOTP(
                new GetOTPModel(mobileno));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                loadingDialog.dismiss();

                int responseCode = response.code();
                Log.e("TAG", "ResponseCode: "+responseCode);

                if(responseCode == 200)
                {
                    CommonUtilities.getToast(mContext,"OTP send successfully on your given number");
                    otpInputDialog();

                }
                else if(responseCode == 500)
                {
                    Log.e("TAG", "ResponseError: "+responseCode);

                    try {
                        String s = response.errorBody().string();
                        JSONObject jsonObject = new JSONObject(s);
                        Log.e("TAG", "ResponseError object: "+jsonObject);
                        String msg = jsonObject.getString("msg");
                        Log.e("TAG", "Msg: "+msg);
                        CommonUtilities.showCommonTextAlert(mContext, msg);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
                else  {
                    CommonUtilities.showCommonTextAlert(mContext, Messages.UNKNOWN_RESPONSE);

                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                Log.e("TAG", "onFailure: "+t.getMessage());
                loadingDialog.dismiss();
            }
        });
    }


    //otp input
    private void otpInputDialog()
    {
        LayoutInflater li = LayoutInflater.from(mContext);
        final View promptsView = li.inflate(R.layout.dialog_forgot, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mContext);
        // set prompts.xml to alertdialog builder
        alertDialogBuilder.setView(promptsView);
        final TextView titalText =  promptsView
                .findViewById(R.id.resetText);

        titalText.setText("Verify OTP");
        final EditText inputText =  promptsView
                .findViewById(R.id.dialog_input);
        inputText.setHint("Enter OTP");
        promptsView.findViewById(R.id.btn_ok).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if(inputText.getText().toString().trim().isEmpty())
                {
                    inputText.setError("Enter OTP");
                }
                else
                {

                    if(CommonUtilities.isOnline(mContext))
                    {
                        alertDialog.cancel();
                        verifyOTP(inputText.getText().toString().trim());
                    }
                    else {
                        CommonUtilities.showCommonTextAlert(mContext, Messages.INTERNET_ALERT);
                    }
                }
            }
        });
        promptsView.findViewById(R.id.btn_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                alertDialog.cancel();
            }
        });
        // create alert dialog
        alertDialog = alertDialogBuilder.create();
        alertDialog.setCancelable(false);
        // show it
        alertDialog.show();
    }



    //veryfy otp
    private void verifyOTP(final String otp)
    {
        loadingDialog = ProgressDialog.show(mContext, "Please wait", "Loading...");
        int custId = SPUtilities.getCustomerId(mContext);
        String token = SPUtilities.getToken(mContext);
        Log.e("ChangePassword", "Token: "+token);

        Call<ResponseBody> call = RetrofitClient.getmInstance().getApi().verifyOTP(
                new OTPVerificationModel(mobileno,otp));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                loadingDialog.dismiss();

                int responseCode = response.code();
                Log.e("TAG", "ResponseCode: "+responseCode);

                if(responseCode == 200)
                {
                    try {
                        String s = response.body().string();
                        Log.e("TAG", "Response: "+s);
                        JSONObject jsonObject=new JSONObject(s);
                        String token = jsonObject.getString("token");
                        int id = jsonObject.getInt("id");
                        String username = jsonObject.getString("username");

                        Log.e("TAG", "Id: "+id);
                        Log.e("Loginctivity", "Token: "+token);

                        SPUtilities.setToken(mContext,token);
                        SPUtilities.setCustomerId(mContext,id);
                        SPUtilities.setLoginStatus(mContext,true);
                        SPUtilities.setUserType(mContext,"GUEST");
                        SPUtilities.setUsername(mContext,username);

                        Intent intent = new Intent(mContext, HomeActivity.class);
                        startActivity(intent);
                        finish();

                    }
                    catch (NullPointerException e){}
                    catch (IOException e) { e.printStackTrace(); }
                    catch (JSONException e) {e.printStackTrace();}

                }
                else if(responseCode == 400)
                {
                    CommonUtilities.showCommonTextAlert(mContext, Messages.OTP_VERIFICATION_FAILED);
                }
                else if(responseCode == 500)
                {
                    Log.e("TAG", "ResponseError: "+responseCode);

                    try {
                        String s = response.errorBody().string();
                        JSONObject jsonObject = new JSONObject(s);
                        Log.e("TAG", "ResponseError object: "+jsonObject);
                        String msg = jsonObject.getString("msg");
                        Log.e("TAG", "Msg: "+msg);
                        CommonUtilities.showCommonTextAlert(mContext, msg);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                else  {
                    CommonUtilities.showCommonTextAlert(mContext, Messages.UNKNOWN_RESPONSE);
                }
            }
            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e("TAG", "onFailure: "+t.getMessage());
                loadingDialog.dismiss();
            }
        });
    }
}