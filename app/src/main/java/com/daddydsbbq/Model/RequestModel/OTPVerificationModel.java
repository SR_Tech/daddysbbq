package com.daddydsbbq.Model.RequestModel;

public class OTPVerificationModel {
    final String phone_number;
    final String verification_code;

    public OTPVerificationModel(String phone_number, String verification_code) {
        this.phone_number = phone_number;
        this.verification_code = verification_code;
    }
}