package com.daddydsbbq.Adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.daddydsbbq.Model.ResponseModel.CartModel;
import com.daddydsbbq.Others.CommonUtilities;
import com.daddydsbbq.R;

import org.json.JSONObject;

import java.util.ArrayList;

 public class CartAdapter extends RecyclerView.Adapter<CartAdapter.ViewHolderClass> {
    private  String TAG = CartAdapter.class.getName();

    public ArrayList<CartModel> mCartModel;
    private Context mContext;
    private ICartMenuDeleteClickListner mICartMenuDeleteClickListner;
    private ICartMenuAddClickListner mICartMenuAddClickListner;
    private ICartMenuMinusClickListner mICartMenuMinusClickListner;

    public CartAdapter(ArrayList<CartModel> mCartModel, Context mContext) {

        this.mCartModel = mCartModel;
        this.mContext = mContext;
        Log.e("TAG", "CategotryModel: "+mCartModel);
    }

    public interface ICartMenuDeleteClickListner
    {
        void onCartItemDeleteClick(View view, int position);
    }
    public void setOnDeleteClickListner(ICartMenuDeleteClickListner mICartMenuDeleteClickListner) {
        this.mICartMenuDeleteClickListner = mICartMenuDeleteClickListner;
    }


    public interface ICartMenuAddClickListner
    {
        void onCartItemAddClick(View view, int position, TextView tv_quantity);
    }
    public void setOnAddClickListner(ICartMenuAddClickListner mICartMenuAddClickListner) {
        this.mICartMenuAddClickListner = mICartMenuAddClickListner;
    }


    public interface ICartMenuMinusClickListner
    {
        void onCartItemMinusClick(View view, int position,TextView tv_quantity);
    }
    public void setOnMinusClickListner(ICartMenuMinusClickListner mICartMenuMinusClickListner) {
        this.mICartMenuMinusClickListner = mICartMenuMinusClickListner;
    }




    public class ViewHolderClass extends RecyclerView.ViewHolder {

        public TextView tv_menu_name;
        public TextView tv_menu_price;
        public TextView tv_quantity;
        //public LinearLayoutCompat ingredient_view;
        public Button delete_button;
        public Button add_button;
        public Button minus_button;
        public ImageView menu_img;
       // public TextView tv_ingredient;





        public ViewHolderClass(View itemView) {
            super(itemView);
            tv_menu_name = itemView.findViewById(R.id.cart_menu_name);
            tv_menu_price = itemView.findViewById(R.id.cart_menu_amount_text);
            tv_quantity = itemView.findViewById(R.id.cart_menu_quantity_text);
            delete_button = itemView.findViewById(R.id.cart_menu_delete_btn);
            add_button = itemView.findViewById(R.id.cart_menu_add_btn);
            minus_button = itemView.findViewById(R.id.cart_menu_minus_btn);
            menu_img = itemView.findViewById(R.id.cart_menu_img);
//            ingredient_view = itemView.findViewById(R.id.cart_ingredient_layout);
//            tv_ingredient = itemView.findViewById(R.id.cart_menu_ingredients_text);

        }
    }

    @Override
    public ViewHolderClass onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        View view = inflater.inflate(R.layout.cart_item_layout,viewGroup,false);
        return new ViewHolderClass(view);
    }

    @Override
    public int getItemCount() {
        return mCartModel.size();
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolderClass viewHolderClass, final int position) {


        viewHolderClass.tv_menu_name.setText(""+ mCartModel.get(position).getCart_menu_name());
        viewHolderClass.tv_menu_price.setText("$"+ CommonUtilities.getFormatedDoubleValue(mCartModel.get(position).getCart_menu_price()));
        viewHolderClass.tv_quantity.setText(""+ mCartModel.get(position).getCart_menu_quantity());

        // Log.e(TAG, "onBindViewHolder: "+mCartModel.get(position).getCart_menu_name() +"\nArray length"+mCartModel.get(position).getIngrredient_array().length()+"");

//        int ingredientLength = mCartModel.get(position).getIngrredient_array().length();
//        if(ingredientLength<1)
//        {
//            viewHolderClass.ingredient_view.setVisibility(View.GONE);
//        }
//        else
//        {
//            try {
//                String ingrdientString="";
//                for (int i = 0; i < ingredientLength; i++) {
//                    JSONObject jsonObject = mCartModel.get(position).getIngrredient_array().getJSONObject(i);
//                    ingrdientString =  (i + 1) +".  "+jsonObject.getString("ingredient_name")+"\n"+
//                            "Price : $"+jsonObject.getDouble("price")+"\n"+
//                            "Qty : "+jsonObject.getInt("quantity")+"\n\n";
//
//
//
//                }
//                viewHolderClass.tv_ingredient.setText(ingrdientString);
//            }
//            catch(Exception e){}
//
//        }
//

        Glide.with(mContext)
                .load(mCartModel.get(position).getCart_menu_url())
//                .placeholder(R.drawable.myplaceholder)
                .error(R.drawable.no_img)
                .into(viewHolderClass.menu_img);
        viewHolderClass.add_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        viewHolderClass.minus_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        viewHolderClass.delete_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mICartMenuDeleteClickListner.onCartItemDeleteClick(v,position);

            }
        });



        viewHolderClass.add_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mICartMenuAddClickListner.onCartItemAddClick(v,position,viewHolderClass.tv_quantity);

            }
        });

        viewHolderClass.minus_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mICartMenuMinusClickListner.onCartItemMinusClick(v,position,viewHolderClass.tv_quantity);

            }
        });

    }



    public void clearData() {
        this.mCartModel = null;
        this.mCartModel = new ArrayList<CartModel>();
    }

}
