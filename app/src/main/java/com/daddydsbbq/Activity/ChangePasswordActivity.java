package com.daddydsbbq.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.daddydsbbq.ApiUtilities.RetrofitClient;
import com.daddydsbbq.Model.RequestModel.ChangePasswordModel;
import com.daddydsbbq.Others.CommonUtilities;
import com.daddydsbbq.Others.Messages;
import com.daddydsbbq.Others.SPUtilities;
import com.daddydsbbq.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChangePasswordActivity extends AppCompatActivity {

    EditText etOldPsw,etNewPsw,etConfirmPsw;
    String mOldPassword, mNewPassword, mConfirmPassword;
    private Context mContext=ChangePasswordActivity.this;
    private Dialog loadingDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);


        etOldPsw  = findViewById(R.id.cp_old_psw);
        etNewPsw  = findViewById(R.id.cp_new_psw);
        etConfirmPsw   = findViewById(R.id.cp_cnfm_psw);

        findViewById(R.id.password_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        findViewById(R.id.cp_updt_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                setValidations();
            }
        });
    }


    private void setValidations()
    {
        mOldPassword = etOldPsw.getText().toString().trim();
        mNewPassword = etNewPsw.getText().toString().trim();
        mConfirmPassword = etConfirmPsw.getText().toString().trim();


        if(mOldPassword.isEmpty())
        {
            CommonUtilities.showCommonTextAlert(mContext,"Enter old password");
        }
        else if(mNewPassword.isEmpty())
        {
            CommonUtilities.showCommonTextAlert(mContext,"Enter new password");
        }
        else if(mConfirmPassword.isEmpty())
        {
            CommonUtilities.showCommonTextAlert(mContext,"Confirm password");

        }
        else if(!mNewPassword.equals(mConfirmPassword))
        {
            CommonUtilities.showCommonTextAlert(mContext,"Password mismatch");

        }else if(mNewPassword.length()<8)
        {
            CommonUtilities.showCommonTextAlert(mContext,"Password length should be min 8 character");
        }
        else
        {
            if(CommonUtilities.isOnline(mContext))
            {
                updatepassword();
            }
            else
            {
                CommonUtilities.showCommonTextAlert(mContext, Messages.INTERNET_ALERT);

            }
        }
    }


    //update password
    private void updatepassword()
    {
        loadingDialog = ProgressDialog.show(mContext, "Please wait", "Loading...");
        int custId = SPUtilities.getCustomerId(mContext);
        String token = SPUtilities.getToken(mContext);
        Log.e("ChangePassword", "Token: "+token);

        Call<ResponseBody> call = RetrofitClient.getmInstance().getApi().changePassword("Token "+token,
                new ChangePasswordModel(mOldPassword, SPUtilities.getUsername(mContext),mNewPassword,mConfirmPassword));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                loadingDialog.dismiss();

                int responseCode = response.code();
                Log.e("TAG", "ResponseCode: "+responseCode);

                if(responseCode == 200)
                {
                    Toast.makeText(mContext, "Password updated successfully.", Toast.LENGTH_SHORT).show();
                    finish();
                }
                else if(responseCode == 401)
                {
                    loadingDialog.dismiss();

                    Log.e("TAG", "ResponseError: "+responseCode);
                    CommonUtilities.sessionExpiredAlert(mContext);


                }
                else if(responseCode == 500)
                {
                    Log.e("TAG", "ResponseError: "+responseCode);

                    try {
                        String s = response.errorBody().string();
                        JSONObject jsonObject = new JSONObject(s);
                        Log.e("TAG", "ResponseError object: "+jsonObject);
                        String msg = jsonObject.getString("msg");
                        Log.e("TAG", "Msg: "+msg);
                        CommonUtilities.showCommonTextAlert(mContext, msg);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
                else  {


                    CommonUtilities.showCommonTextAlert(mContext, Messages.UNKNOWN_RESPONSE);

                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                Log.e("TAG", "onFailure: "+t.getMessage());
                loadingDialog.dismiss();
            }
        });
    }

}