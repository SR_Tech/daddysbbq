package com.daddydsbbq.ApiUtilities;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClient {

    //prod
    private static final String Base_URL = "https://restaurant60-be-prod-xtpocjmkpa-uw.a.run.app/";
   // dev url
    // //private static final String Base_URL = "https://restaurant60-be-dev-xtpocjmkpa-uw.a.run.app/";

    private static RetrofitClient mInstance;
    public static Retrofit retrofit=null;
    private RetrofitClient()
    {
        retrofit = new Retrofit.Builder()
                .baseUrl(Base_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    public static synchronized RetrofitClient getmInstance()
    {
        if(mInstance==null)
        {
            mInstance=new RetrofitClient();
        }
        return mInstance;
    }
    public Api getApi(){
        return retrofit.create(Api.class);
    }
}
