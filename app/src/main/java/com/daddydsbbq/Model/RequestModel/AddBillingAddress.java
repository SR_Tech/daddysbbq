package com.daddydsbbq.Model.RequestModel;

public class AddBillingAddress {
    final String company_name;
    final String country;
    final String house_number;
    final String city;
    final String zip;
    final String address;
    final int customer_id;
    final String state;
    final Integer priority;
    final String name;


    public AddBillingAddress(String company_name, String country, String house_number, String city, String zip, String address, int customer_id, String state, Integer priority, String name) {
        this.company_name = company_name;
        this.country = country;
        this.house_number = house_number;
        this.city = city;
        this.zip = zip;
        this.address = address;
        this.customer_id = customer_id;
        this.state = state;
        this.priority = priority;
        this.name = name;
    }
}
