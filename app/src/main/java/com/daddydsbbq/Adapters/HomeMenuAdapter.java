package com.daddydsbbq.Adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.daddydsbbq.Model.ResponseModel.HomeMenuModel;
import com.daddydsbbq.R;

import java.util.ArrayList;

public class HomeMenuAdapter extends BaseAdapter {

    Context context;

    public ArrayList<HomeMenuModel> mCategotryModel;


    LayoutInflater inflater;

    public HomeMenuAdapter(Context context, ArrayList<HomeMenuModel> mCategotryModel) {
        this.context = context;
        this.mCategotryModel = mCategotryModel;
        Log.e("TAG", "HomeMenuAdapter: Menu"+mCategotryModel.get(0).getCategory_name());
    }

    @Override
    public int getCount() {
        return mCategotryModel.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (inflater == null)
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null){

            convertView = inflater.inflate(R.layout.home_menu_layout,null);

        }

        ImageView imageView = convertView.findViewById(R.id.home_menu_image);
        TextView textView = convertView.findViewById(R.id.home_menu_name);

       // imageView.setImageResource(MenuImage[position]);
        textView.setText(""+ mCategotryModel.get(position).getCategory_name());
        Glide.with(context)
                .load(mCategotryModel.get(position).getCategory_url())
//                .placeholder(R.drawable.myplaceholder)
                .error(R.drawable.logo)
                .into(imageView);

        return convertView;
    }
}