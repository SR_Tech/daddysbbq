package com.daddydsbbq.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import com.daddydsbbq.ApiUtilities.RetrofitClient;
import com.daddydsbbq.Model.RequestModel.UpdateProfileModel;
import com.daddydsbbq.Others.CommonUtilities;
import com.daddydsbbq.Others.Messages;
import com.daddydsbbq.Others.SPUtilities;
import com.daddydsbbq.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfileActivity extends AppCompatActivity {


    String TAG = ProfileActivity.class.getName();
    Context mContext = ProfileActivity.this;
    public Dialog loadingDialog;
    EditText etSalutation,etFname,etLname,etEmail,etCountry,etMobile;
    String mSalutation,mUsername,mFName,mLName,mEmail,mCountryCode,mMobile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        if(CommonUtilities.isOnline(mContext))
        {
            getProfile();
        }
        else
        {

            CommonUtilities.showCommonTextAlert(mContext, Messages.INTERNET_ALERT);
        }
        setViewId();
    }
    private void setViewId()
    {
        etSalutation = findViewById(R.id.profile_salutation);
        etFname =  findViewById(R.id.profile_fname);
        etLname =  findViewById(R.id.profile_lname);
        etEmail = findViewById(R.id.profile_email);
        etCountry = findViewById(R.id.profile_country);
        etMobile = findViewById(R.id.profile_mobile);

        findViewById(R.id.profile_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        findViewById(R.id.profile_update).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                setValidation();

            }
        });

        findViewById(R.id.profile_change_pass).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, ChangePasswordActivity.class);
                startActivity(intent);
            }
        });
    }
    private void setValidation()
    {

        mSalutation   =       etSalutation.getText().toString().trim();
        mFName        =       etFname.getText().toString().trim();
        mLName        =       etLname.getText().toString().trim();
        mEmail        =       etEmail.getText().toString().trim();
        mCountryCode  =       etCountry.getText().toString().trim();
        mMobile       =       etMobile.getText().toString().trim();

        if(mFName.isEmpty())
        {
            CommonUtilities.showCommonTextAlert(mContext,"Enter first name");
        }else if(mLName.isEmpty())
        {
            CommonUtilities.showCommonTextAlert(mContext,"Enter last name");

        }else if(mEmail.isEmpty())
        {
            CommonUtilities.showCommonTextAlert(mContext,"Enter email address");

        }
        else if(!CommonUtilities.isValidEmail(mEmail))
        {
            CommonUtilities.showCommonTextAlert(mContext,"Enter valid email address");

        }else if(mMobile.isEmpty())
        {
            CommonUtilities.showCommonTextAlert(mContext,"Enter mobile number");

        }

        else
        {

            if(CommonUtilities.isOnline(mContext))
            {
                updateProfile();
            }
            else
            {
                CommonUtilities.showCommonTextAlert(mContext, Messages.INTERNET_ALERT);
            }
        }

    }


    //get profile
    private void getProfile()
    {
        loadingDialog = ProgressDialog.show(mContext, "Please wait", "Loading...");

        String token = SPUtilities.getToken(mContext);
        Log.e(TAG, "Token: "+token);

        int custId = SPUtilities.getCustomerId(mContext);
        Call<ResponseBody> call = RetrofitClient.getmInstance().getApi().getProfile("Token "+token,""+custId);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                loadingDialog.dismiss();
                int responseCode = response.code();
                Log.e("TAG", "ResponseCode: "+responseCode);

                if(responseCode == 200)
                {
                    try {
                        String s = response.body().string();
                        Log.e("TAG", "Response: "+s);
                        JSONObject jsonObject=new JSONObject(s);
                        JSONArray resultsArray = jsonObject.getJSONArray("results");

                        if(resultsArray.length() <= 0)
                        {
                            CommonUtilities.showCommonTextAlert(mContext, "The system is temporarily unavailable. Please try again later");

                        }
                        else
                        {

                            JSONObject resultJsonObject = resultsArray.getJSONObject(0);
                            etSalutation.setText(resultJsonObject.getString("salutation"));
                            JSONObject dataObject = resultJsonObject.getJSONObject("customer");
                            Log.e("TAG", "Response(Customer): "+dataObject);
                            mUsername = dataObject.getString("username");
                            SPUtilities.setUsername(mContext,mUsername);
                            etFname.setText(dataObject.getString("first_name"));
                            etLname.setText(dataObject.getString("last_name"));
                            etEmail.setText(dataObject.getString("email"));
                            String phoneno = resultJsonObject.getString("phone_number");

                            if(phoneno.contains("+91"))
                            {
                                etCountry.setText("+91");
                                String mbl = phoneno.replace("+91","");
                                Log.e("TAG", "Mobile = : "+mbl);

                                etMobile.setText(mbl);
                            }
                            else if(phoneno.contains("+1"))
                            {
                                etCountry.setText("+1");
                                String mbl = phoneno.replace("+1","");
                                Log.e("TAG", "Mobile = : "+mbl);

                                etMobile.setText(mbl);
                            }



                        }

                    }
                    catch (NullPointerException e){}
                    catch (IOException e) { e.printStackTrace(); }
                    catch (JSONException e) {e.printStackTrace();}

                }
                else if(responseCode == 401)
                {
                    Log.e("TAG", "ResponseError: "+responseCode);
                    CommonUtilities.sessionExpiredAlert(mContext);

                }
                else if(responseCode == 500)
                {
                    Log.e("TAG", "ResponseError: "+responseCode);

                    try {
                        String s = response.errorBody().string();
                        JSONObject jsonObject = new JSONObject(s);
                        Log.e("TAG", "ResponseError object: "+jsonObject);
                        String msg = jsonObject.getString("msg");
                        Log.e("TAG", "Msg: "+msg);
                        CommonUtilities.showCommonTextAlert(mContext, msg);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
                else  {

                    CommonUtilities.showCommonTextAlert(mContext, Messages.UNKNOWN_RESPONSE);

                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                Log.e("TAG", "onFailure: "+t.getMessage());
                loadingDialog.dismiss();
            }
        });
    }

    //update profile
    private void updateProfile()
    {
        loadingDialog = ProgressDialog.show(mContext, "Please wait", "Loading...");


        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        String dateTime = simpleDateFormat.format(calendar.getTime()).toString();
        Log.e(TAG, "Created time: "+dateTime);

        String custToken = SPUtilities.getToken(mContext);
        int custId = SPUtilities.getCustomerId(mContext);
        Call<ResponseBody> call = RetrofitClient.getmInstance().getApi().updateProfile("Token "+custToken,""+custId,
                new UpdateProfileModel(mSalutation,mFName,mLName,mUsername,mEmail,dateTime,mCountryCode+mMobile));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                int responseCode = response.code();
                Log.e("TAG", "ResponseCode: "+responseCode);

                if(responseCode == 200)
                {
                    updateCustomerMobile();

                }
                else if(responseCode == 401)
                {
                    loadingDialog.dismiss();

                    Log.e("TAG", "ResponseError: "+responseCode);
                    CommonUtilities.sessionExpiredAlert(mContext);


                }
                else if(responseCode == 500)
                {
                    Log.e("TAG", "ResponseError: "+responseCode);
                    loadingDialog.dismiss();

                    try {
                        String s = response.errorBody().string();
                        Log.e("TAG", "Response: " + s);
                        JSONObject jsonObject = new JSONObject(s);
                        String msg = jsonObject.getString("msg");
                        Log.e("TAG", "Msg: " + msg);
                        CommonUtilities.showCommonTextAlert(mContext, msg);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
                else  {

                    loadingDialog.dismiss();

                    CommonUtilities.showCommonTextAlert(mContext, Messages.UNKNOWN_RESPONSE);

                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                Log.e("TAG", "onFailure: "+t.getMessage());
                loadingDialog.dismiss();
            }
        });
    }

    //update mobile
    private void updateCustomerMobile()
    {
        //loadingDialog = ProgressDialog.show(mContext, "Please wait", "Loading...");


        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        String dateTime = simpleDateFormat.format(calendar.getTime()).toString();
        Log.e("TAG", "Created time: "+dateTime);

        String token = SPUtilities.getToken(mContext);
        int custId = SPUtilities.getCustomerId(mContext);
        Call<ResponseBody> call = RetrofitClient.getmInstance().getApi().updateCustomerMobile("Token "+token,""+custId,
                new UpdateProfileModel(mSalutation,mFName,mLName,mUsername,mEmail,dateTime,mCountryCode+mMobile));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                loadingDialog.dismiss();
                int responseCode = response.code();
                Log.e("TAG", "ResponseCode: "+responseCode);

                if(responseCode == 200)
                {

                    CommonUtilities.showCommonTextAlert(mContext,"Profile updated successfully");
                }
                else if(responseCode == 401)
                {
                    Log.e("TAG", "ResponseError: "+responseCode);
                    CommonUtilities.sessionExpiredAlert(mContext);

                }
                else if(responseCode == 500)
                {
                    Log.e("TAG", "ResponseError: "+responseCode);

                    try {
                        String s = response.errorBody().string();
                        Log.e("TAG", "Response: " + s);
                        JSONObject jsonObject = new JSONObject(s);
                        String msg = jsonObject.getString("msg");
                        Log.e("TAG", "Msg: " + msg);
                        CommonUtilities.showCommonTextAlert(mContext, msg);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
                else  {

                    CommonUtilities.showCommonTextAlert(mContext, Messages.UNKNOWN_RESPONSE);

                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                Log.e("TAG", "onFailure: "+t.getMessage());
                loadingDialog.dismiss();
            }
        });
    }
}