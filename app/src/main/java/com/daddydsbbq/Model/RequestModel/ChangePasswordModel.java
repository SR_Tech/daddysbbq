package com.daddydsbbq.Model.RequestModel;

public class ChangePasswordModel {
    final String old_password;
    final String username;
    final String new_password1;
    final String new_password2;

    public ChangePasswordModel(String old_password, String username, String new_password1, String new_password2) {
        this.old_password = old_password;
        this.username = username;
        this.new_password1 = new_password1;
        this.new_password2 = new_password2;
    }
}
