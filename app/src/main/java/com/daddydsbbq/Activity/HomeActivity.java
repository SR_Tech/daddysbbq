package com.daddydsbbq.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import com.daddydsbbq.Adapters.HomeMenuAdapter;
import com.daddydsbbq.ApiUtilities.RetrofitClient;
import com.daddydsbbq.Model.RequestModel.LoginModel;
import com.daddydsbbq.Model.ResponseModel.HomeMenuModel;
import com.daddydsbbq.Others.AppUtilities;
import com.daddydsbbq.Others.CommonUtilities;
import com.daddydsbbq.Others.Messages;
import com.daddydsbbq.Others.SPUtilities;
import com.daddydsbbq.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeActivity extends AppCompatActivity {

    GridView gridView;
    Context mContext;
    private  String TAG = HomeActivity.class.getName();
    private Dialog loadingDialog;
    private  String restaurant_status;
    ArrayList<HomeMenuModel> mCategotryModel;
    HomeMenuAdapter mCategoryAdapter;

    private TextView txCartCount;


   @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        mContext = HomeActivity.this;
        mCategotryModel = new ArrayList<>();


       txCartCount= findViewById(R.id.home_cart_count);
       txCartCount.setText("0");
       gridView = findViewById(R.id.home_gridview);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(mContext, MenuActivity.class);
                intent.putExtra("cat_id", mCategotryModel.get(position).getCategory_id() );
              startActivity(intent);

            }
        });

       txCartCount.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               Intent intent = new Intent(mContext, CartActivity.class);
               startActivity(intent);
           }
       });
       findViewById(R.id.cartlayout).setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               Intent intent = new Intent(mContext, CartActivity.class);
               startActivity(intent);
           }
       });

        if(CommonUtilities.isOnline(mContext)) {


            //String token = SharePreferenceUtilities.getToken(mContext);
            boolean login_status = SPUtilities.getLoginStatus(mContext);
            if (!login_status) {
                getAdminToken();

            } else {
//                getTiming();
//                getOfferMenu();
                  getCategoryList();
                checkCartIsPresent();

            }
        }else
        {
            CommonUtilities.showCommonTextAlert(mContext,Messages.INTERNET_ALERT);
        }
        buttonClicks();

    }

    @Override
    protected void onResume() {
        super.onResume();

        if (CommonUtilities.isOnline(mContext)) {
//            checkCartIsPresent();
            getCartCount();
        } else {
            CommonUtilities.showCommonTextAlert(mContext, Messages.INTERNET_ALERT);
        }
    }

    private void buttonClicks() {
        findViewById(R.id.order_list).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        findViewById(R.id.drawer_menu).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, DrawerMenuActivity.class);
                startActivity(intent);
            }
        });
    }

    //call get AdminToken
    public void getAdminToken()
    {

        loadingDialog = ProgressDialog.show(mContext, "Please wait", "Loading...");


        Call<ResponseBody> call = RetrofitClient.getmInstance().getApi().login(new LoginModel(AppUtilities.adminusername,AppUtilities.adminpassword, AppUtilities.RESTAURANT_ID));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                loadingDialog.dismiss();


                int responseCode = response.code();
                Log.e("TAG", "ResponseCode: "+responseCode);

                if(responseCode == 200)
                {

                    try {
                        String s = response.body().string();
                        Log.e("TAG", "Response: "+s);
                        JSONObject jsonObject=new JSONObject(s);
                        String token = jsonObject.getString("token");

                        Log.e("TAG", "Token: "+token);
                        SPUtilities.setToken(mContext,token);

//                        getTiming();
//                        getBestSellerMenu();
//                        getOfferMenu();

                        getCategoryList();


                    }
                    catch (NullPointerException e){}
                    catch (IOException e) { e.printStackTrace(); }
                    catch (JSONException e) {e.printStackTrace();}

                }
                else if(responseCode == 401)
                {
                    Log.e("TAG", "ResponseError: "+responseCode);

                    //  CommonUtilities.commonAlert(mContext, "Username or password is incorrect");

                }
                else if(responseCode == 500)
                {
                    Log.e("TAG", "ResponseError: "+responseCode);

                    try {
                        String s = response.errorBody().string();
                        JSONObject jsonObject = new JSONObject(s);
                        //Log.e("TAG", "ResponseError object: "+jsonObject);
                        String msg = jsonObject.getString("msg");
                        Log.e("TAG", "Msg: "+msg);
                        CommonUtilities.showCommonTextAlert(mContext, msg);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
                else  {

                    CommonUtilities.showCommonTextAlert(mContext, "The system is temporarily unavailable. Please try again later");

                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                Log.e("TAG", "onFailure: "+t.getMessage());
                loadingDialog.dismiss();
            }
        });
    }

    private void getCategoryList()
    {
        loadingDialog = ProgressDialog.show(mContext, "Please wait", "Loading...");

        String token = SPUtilities.getToken(mContext);

        Call<ResponseBody> call = RetrofitClient.getmInstance().getApi().getCategory("Token "+token,""+ AppUtilities.RestaurantGlobalid);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                loadingDialog.dismiss();
                int responseCode = response.code();
                Log.e("TAG", "ResponseCode: "+responseCode);

                if(responseCode == 200)
                {
                    try {
                        String s = response.body().string();
                        JSONArray jsonArray=new JSONArray(s);
                       Log.e("TAG", "Response Data: "+jsonArray);
                        if(jsonArray.length()>0) {

                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject = jsonArray.getJSONObject(i);
                                HomeMenuModel categotryModel = new HomeMenuModel();
                                categotryModel.setCategory_id(jsonObject.getInt("category_id"));
                                categotryModel.setCategory_name(jsonObject.getString("category"));
                                categotryModel.setCategory_url(jsonObject.getString("category_url"));

                                Log.e(TAG, "\n\n\nonResponse: cat"+jsonObject.getString("category")+"\n\n\n\n\n\n" );
                                mCategotryModel.add(categotryModel);
                            }
                            Log.e(TAG, "onResponse: model size"+mCategotryModel.size() );
                            setCategoryAdapter();
                        }

                    }
                    catch (NullPointerException e){}
                    catch (IOException e) { e.printStackTrace(); }
                    catch (JSONException e) {e.printStackTrace();}

                }
                else if(responseCode == 401)
                {
                    Log.e("TAG", "ResponseError: "+responseCode);
                    CommonUtilities.sessionExpiredAlert(mContext);

                }
                else if(responseCode == 500)
                {
                    Log.e("TAG", "ResponseError: "+responseCode);

                    try {
                        String s = response.errorBody().string();
                        JSONObject jsonObject = new JSONObject(s);
                        Log.e("TAG", "ResponseError object: "+jsonObject);
                        String msg = jsonObject.getString("msg");
                        Log.e("TAG", "Msg: "+msg);
                        CommonUtilities.showCommonTextAlert(mContext, msg);

                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.e("TAG", "exception: ");

                    } catch (IOException e) {
                        e.printStackTrace();
                        Log.e("TAG", "exception: ");

                    }

                }
                else  {

                    CommonUtilities.showCommonTextAlert(mContext, Messages.UNKNOWN_RESPONSE);

                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                Log.e("TAG", "onFailure: "+t.getMessage());
                loadingDialog.dismiss();
            }
        });
    }

    void setCategoryAdapter()
    {
        Log.e("TAG test", "HomeMenuAdapter: Menu"+mCategotryModel.get(0));
        mCategoryAdapter = new HomeMenuAdapter(mContext, mCategotryModel);
        gridView.setAdapter(mCategoryAdapter);
    }

    //checkCartIs Present or not
    public void checkCartIsPresent()
    {
       // loadingDialog = ProgressDialog.show(mContext, "Please wait", "Loading...");

        String token = SPUtilities.getToken(mContext);
        int custId = SPUtilities.getCustomerId(mContext);
        Call<ResponseBody> call = RetrofitClient.getmInstance().getApi().checkCartIsCreated("Token "+token,""+custId, AppUtilities.RestaurantGlobalid);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                loadingDialog.dismiss();
                int responseCode = response.code();
                Log.e("TAG", "ResponseCode: "+responseCode);

                if(responseCode == 200)
                {
                    try {
                        String s = response.body().string();
                        Log.e("TAG", "Cart Response 1: "+s);
                        JSONObject jsonObject=new JSONObject(s);
                        JSONArray resultsArray = jsonObject.getJSONArray("results");

                        if(resultsArray.length() <= 0)
                        {
                            create_cart();

                        }
                        else
                        {

                            JSONObject resultJsonObject = resultsArray.getJSONObject(0);

                            int cartId = resultJsonObject.getInt("id");
                            int storeWRTCart = resultJsonObject.getInt("restaurant");
                            SPUtilities.setCartId(mContext,cartId);
                            SPUtilities.setStoreidWrtCart(mContext,storeWRTCart);
//
                            getCartCount();
                        }

                    }
                    catch (NullPointerException e){}
                    catch (IOException e) { e.printStackTrace(); }
                    catch (JSONException e) {e.printStackTrace();}

                }
                else if(responseCode == 401)
                {
                    Log.e(TAG, "ResponseError: "+responseCode);
                    CommonUtilities.sessionExpiredAlert(mContext);

                }
                else if(responseCode == 500)
                {
                    Log.e(TAG, "ResponseError: "+responseCode);

                    try {
                        String s = response.errorBody().string();
                        JSONObject jsonObject = new JSONObject(s);
                        Log.e(TAG, "ResponseError object: "+jsonObject);
                        String msg = jsonObject.getString("msg");
                        Log.e(TAG, "Msg: "+msg);
                        CommonUtilities.showCommonTextAlert(mContext, msg);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
                else  {

                    CommonUtilities.showCommonTextAlert(mContext, "The system is temporarily unavailable. Please try again later");

                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                Log.e("TAG", "onFailure: "+t.getMessage());
                loadingDialog.dismiss();
            }
        });
    }

    // Create cart if not created
    private void create_cart()
    {
     //   loadingDialog = ProgressDialog.show(mContext, "Please wait", "Loading...");

        String token = SPUtilities.getToken(mContext);
        int custId = SPUtilities.getCustomerId(mContext);
        Call<ResponseBody> call = RetrofitClient.getmInstance().getApi().createCart("Token "+token,""+custId, AppUtilities.RestaurantGlobalid);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                loadingDialog.dismiss();

                int responseCode = response.code();
                Log.e("TAG", "ResponseCode: "+responseCode);

                if(responseCode == 201)
                {
                    try {
                        String s = response.body().string();
                        // Log.e("TAG", "Response: "+s);
                        JSONObject jsonObject=new JSONObject(s);

                        Log.e("TAG", "Response: "+s);

                        //JSONArray resultsArray = jsonObject.getJSONArray("results");

                        if(jsonObject.length() <= 0)
                        {

                            CommonUtilities.showCommonTextAlert(mContext, Messages.CART_NOT_CREATED);
                        }
                        else
                        {

                            int cartId = jsonObject.getInt("id");
                            int storeWRTCart = jsonObject.getInt("restaurant");

                            SPUtilities.setCartId(mContext,cartId);
                            SPUtilities.setStoreidWrtCart(mContext,storeWRTCart);
//
                            getCartCount();
                        }

                    }
                    catch (NullPointerException e){}
                    catch (IOException e) { e.printStackTrace(); }
                    catch (JSONException e) {e.printStackTrace();}

                }
                else if(responseCode == 401)
                {
                    Log.e("TAG", "ResponseError: "+responseCode);
                    CommonUtilities.sessionExpiredAlert(mContext);

                }
                else if(responseCode == 500)
                {
                    Log.e("TAG", "ResponseError: "+responseCode);

                    try {
                        String s = response.errorBody().string();
                        JSONObject jsonObject = new JSONObject(s);
                        Log.e("TAG", "ResponseError object: "+jsonObject);
                        String msg = jsonObject.getString("msg");
                        Log.e("TAG", "Msg: "+msg);
                        CommonUtilities.showCommonTextAlert(mContext, msg);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
                else  {

                    CommonUtilities.showCommonTextAlert(mContext, "The system is temporarily unavailable. Please try again later");

                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                Log.e("TAG", "onFailure: "+t.getMessage());
                loadingDialog.dismiss();
            }
        });
    }

    //get cart count
    private void getCartCount()
    {
        //  String token = SharePreferenceUtilities.getToken(mContext);
        String graphqlToken = AppUtilities.globGraphQlToken;
        String customerid = String.valueOf(SPUtilities.getCustomerId(mContext));
        String restid = AppUtilities.RestaurantGlobalid;
        MediaType mediaType = MediaType.parse("application/json");
        String ss  = "{\"query\":\"query cartItemCount ($token: String, $customerId: Int, $restaurantId: Int) {\\n  cartItemCount (token : $token,customerId: $customerId, restaurantId: $restaurantId) {\\n    count\\n  }\\n}\",\"variables\":{\"token\":\"0o6jcui8mfhmp56we69kcmu5rkejtock\",\"customerId\":"+customerid+",\"restaurantId\":"+restid+"}}";

        //  String s2 = "{\"query\":\"query cartItemCount (\$token: String, \$customerId: Int,\$restaurantId: Int) {\\n  cartItemCount (token : \$token,customerId: \$customerId,restaurantId: \$restaurantId ) {\\n    count\\n  }\\n}\",\"variables\":{\"token\":\"0o6jcui8mfhmp56we69kcmu5rkejtock\",\"customerId\":"+customerid+",\"restaurantId\":\"$restid\"}}";
        RequestBody requestBody = RequestBody.create(mediaType,ss);

        Log.e(TAG, "getCartCount request string: "+ss);

        Call<ResponseBody> call = RetrofitClient.getmInstance().getApi().getCartCount(requestBody);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                int responseCode = response.code();
                Log.e("TAG", "ResponseCode: "+responseCode);

                if(responseCode == 200)
                {
                    try {
                        String s = response.body().string();
                        // Log.e("TAG", "Response: "+s);
                        JSONObject jsonObject=new JSONObject(s);
                        JSONObject dataObject = jsonObject.getJSONObject("data");
                        JSONObject countObject = dataObject.getJSONObject("cartItemCount");
                        int cartCount = countObject.getInt("count");
                        txCartCount.setText(""+cartCount);
                        SPUtilities.setCartCount(mContext,cartCount);

                        Log.e(TAG, "Cart Count Response: "+s);

                    }
                    catch (NullPointerException e){}
                    catch (IOException e) { e.printStackTrace(); }
                    catch (JSONException e) {e.printStackTrace();}

                }
                else if(responseCode == 401)
                {
                    Log.e("TAG", "ResponseError: "+responseCode);
                    CommonUtilities.sessionExpiredAlert(mContext);

                }
                else if(responseCode == 400)

                {
                    Log.e(TAG, "ResponseError: "+responseCode);

                    try {
                        String s = response.errorBody().string();
                        JSONObject jsonObject = new JSONObject(s);
                        Log.e(TAG, "ResponseError object: "+jsonObject);
                        String msg = jsonObject.getString("msg");
                        Log.e(TAG, "Msg: "+msg);
                        CommonUtilities.showCommonTextAlert(mContext, msg);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
                else if(responseCode == 500)
                {
                    Log.e("TAG", "ResponseError: "+responseCode);

                    try {
                        String s = response.errorBody().string();
                        JSONObject jsonObject = new JSONObject(s);
                        Log.e("TAG", "ResponseError object: "+jsonObject);
                        String msg = jsonObject.getString("msg");
                        Log.e("TAG", "Msg: "+msg);
                        CommonUtilities.showCommonTextAlert(mContext, msg);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
                else  {

                    CommonUtilities.showCommonTextAlert(mContext, "The system is temporarily unavailable. Please try again later");

                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                Log.e("TAG", "onFailure: "+t.getMessage());
            }
        });
    }



}