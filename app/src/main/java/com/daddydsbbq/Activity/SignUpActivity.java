package com.daddydsbbq.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.daddydsbbq.ApiUtilities.RetrofitClient;
import com.daddydsbbq.Model.RequestModel.GetUserRegisrationIdModel;
import com.daddydsbbq.Model.RequestModel.UserRegistrationModel;
import com.daddydsbbq.Others.AppUtilities;
import com.daddydsbbq.Others.CommonUtilities;
import com.daddydsbbq.Others.Messages;
import com.daddydsbbq.Others.SPUtilities;
import com.daddydsbbq.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignUpActivity extends AppCompatActivity {

    EditText edtSalutation, edtUsername,edtFname,edtLname,edtEmail,edtCountrycode,edtMobile,edtPassword,edtConfirmPassword;
    String mSalutation,mUsername,mFName,mLName,mEmail,mCountryCode,mMobile,mPassword,mConfiirmPassword;
    Button signup;
    Context mContext = SignUpActivity.this;
    public Dialog loadingDialog;
    private AlertDialog alertDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        setViews();
    }

    void setViews() {
        edtSalutation = findViewById(R.id.edt_rsalutation);
        edtSalutation.setFocusable(false);
        edtUsername  = findViewById(R.id.edt_ruser_name);
        edtFname  = findViewById(R.id.edt_rf_name);
        edtLname  = findViewById(R.id.edt_rl_name);
        edtEmail  = findViewById(R.id.edt_remail);
        edtCountrycode  = findViewById(R.id.edt_code);
        edtMobile  = findViewById(R.id.edt_rm_no);
        edtPassword  = findViewById(R.id.edt_rpassword);
        edtConfirmPassword  = findViewById(R.id.edt_c_password);
        findViewById(R.id.btn_sign_up).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setValidation();
            }
        });

        edtSalutation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder builderSingle = new AlertDialog.Builder(mContext);
                builderSingle.setTitle("Select Salutation");

                final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(mContext, android.R.layout.select_dialog_singlechoice);
                arrayAdapter.add("Mr.");
                arrayAdapter.add("Mrs.");
                arrayAdapter.add("Miss.");
                arrayAdapter.add("Ms.");

                builderSingle.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

                builderSingle.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String strName = arrayAdapter.getItem(which);
                        edtSalutation.setText(strName);
                    }
                });
                builderSingle.show();

            }
        });

        findViewById(R.id.txt_already_registered).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }


    //set Validations
    private void setValidation()
    {
        mSalutation   =       edtSalutation.getText().toString().trim();
        mUsername     =       edtUsername.getText().toString().trim();
        mFName        =       edtFname.getText().toString().trim();
        mLName        =       edtLname.getText().toString().trim();
        mEmail        =       edtEmail.getText().toString().trim();
        mCountryCode  =       edtCountrycode.getText().toString().trim();
        mMobile       =       edtMobile.getText().toString().trim();
        mPassword     =       edtPassword.getText().toString().trim();
        mConfiirmPassword =   edtConfirmPassword.getText().toString().trim();

        if(mSalutation.isEmpty())
        {
            CommonUtilities.showCommonTextAlert(mContext,"Select salutation");
        }else if(mUsername.isEmpty())
        {
            CommonUtilities.showCommonTextAlert(mContext,"Enter username");


        }else if(mFName.isEmpty())
        {
            CommonUtilities.showCommonTextAlert(mContext,"Enter first name");
        }else if(mLName.isEmpty())
        {
            CommonUtilities.showCommonTextAlert(mContext,"Enter last name");

        }else if(mEmail.isEmpty())
        {
            CommonUtilities.showCommonTextAlert(mContext,"Enter email address");

        }
        else if(!CommonUtilities.isValidEmail(mEmail))
        {
            CommonUtilities.showCommonTextAlert(mContext,"Enter valid email address");

        }else if(mMobile.isEmpty())
        {
            CommonUtilities.showCommonTextAlert(mContext,"Enter mobile number");

        }else if(mCountryCode.isEmpty())
        {
            CommonUtilities.showCommonTextAlert(mContext,"Enter country code");

        }else if(mPassword.isEmpty())
        {
            CommonUtilities.showCommonTextAlert(mContext,"Enter password");

        }
        else if(mPassword.length()<8)
        {
            CommonUtilities.showCommonTextAlert(mContext,"Password lenngth should be minimum 8 character");

        }else if(mConfiirmPassword.isEmpty())
        {
            CommonUtilities.showCommonTextAlert(mContext,"Confirm password");

        }
        else if(!mConfiirmPassword.equals(mPassword))
        {
            CommonUtilities.showCommonTextAlert(mContext,"Password mismatch");

        }
        else
        {
            if(CommonUtilities.isOnline(mContext))
            {
                getUserRegistrationId();
            }
            else
            {
                CommonUtilities.showCommonTextAlert(mContext, Messages.INTERNET_ALERT);

            }
        }

    }


    //get user registration id
    private void getUserRegistrationId()
    {

        loadingDialog = ProgressDialog.show(mContext, "Please wait", "Loading...");

        String token = SPUtilities.getToken(mContext);
        Call<ResponseBody> call = RetrofitClient.getmInstance().getApi().getUserRegId(
                "Token "+token, new GetUserRegisrationIdModel(mUsername,mEmail,mFName,mLName,mPassword,"N", AppUtilities.RESTAURANT_ID));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                loadingDialog.dismiss();
                int responseCode = response.code();
                Log.e("TAG", "ResponseCode: "+responseCode);

                if(responseCode == 200)
                {
                    try {
                        String s = response.body().string();
                        Log.e("TAG", "Response: "+s);
                        JSONObject jsonObject=new JSONObject(s);
                        String regId = jsonObject.getString("id");
                        userRegistration(regId);



                    }
                    catch (NullPointerException e){}
                    catch (IOException e) { e.printStackTrace(); }
                    catch (JSONException e) {e.printStackTrace();}

                }
                else if(responseCode == 400)
                {
                    Log.e("TAG", "ResponseError: "+responseCode);
                    CommonUtilities.showCommonTextAlert(mContext, "User exist in system with same email or username");
                }else if(responseCode == 500)
                {
                    try {
                        String s = response.errorBody().string();
                        JSONObject jsonObject = new JSONObject(s);
                        Log.e("TAG", "ResponseError object: "+jsonObject);
                        String msg = jsonObject.getString("msg");
                        Log.e("TAG", "Msg: "+msg);
                        CommonUtilities.showCommonTextAlert(mContext, msg);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                else  {
                    CommonUtilities.showCommonTextAlert(mContext, "The system is temporarily unavailable. Please try again later");
                }
            }
            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                Log.e("TAG", "onFailure: "+t.getMessage());
                loadingDialog.dismiss();
            }
        });

    }


    private void userRegistration(String customerId)
    {

        loadingDialog = ProgressDialog.show(mContext, "Please wait", "Loading...");

        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        String dateTime = simpleDateFormat.format(calendar.getTime()).toString();
        Log.e("TAG", "Created time: "+dateTime);

        String token = SPUtilities.getToken(mContext);
        Call<ResponseBody> call = RetrofitClient.getmInstance().getApi().userRegistration(
                "Token "+token, new UserRegistrationModel(customerId,dateTime,"extra",mSalutation,mMobile,mFName,mLName, mEmail));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                loadingDialog.dismiss();
                int responseCode = response.code();
                Log.e("TAG", "ResponseCode: " + responseCode);

                if (responseCode == 200) {
                    try {
                        String s = response.body().string();
                        Log.e("TAG", "Response: " + s);
                        JSONObject jsonObject = new JSONObject(s);
                        //int regId = jsonObject.getInt("id");


                        LayoutInflater li = LayoutInflater.from(mContext);
                        final View promptsView = li.inflate(R.layout.common_alert_layout, null);
                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mContext);
                        // set prompts.xml to alertdialog builder
                        alertDialogBuilder.setView(promptsView);
                        final TextView titalText = promptsView
                                .findViewById(R.id.alert_msg);
                        titalText.setText(Messages.REGISTRATION_SUCCESSFULL_TITAL+" "+Messages.REGISTRATION_SUCCESSFULL);
                        promptsView.findViewById(R.id.btn_ok).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                alertDialog.cancel();
                                finish();

                            }
                        });
                        // create alert dialog
                        alertDialog = alertDialogBuilder.create();
                        alertDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                        alertDialog.show();





                    } catch (NullPointerException e) {
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else if (responseCode == 400) {
                    Log.e("TAG", "ResponseError: " + responseCode);
                    CommonUtilities.showCommonTextAlert(mContext, "User exist in system with same email or username");
                }else if (responseCode == 500) {
                    Log.e("TAG", "ResponseError: " + responseCode);
                    try {
                        String s = response.errorBody().string();
                        JSONObject jsonObject = new JSONObject(s);
                        Log.e("TAG", "ResponseError object: "+jsonObject);
                        String msg = jsonObject.getString("msg");
                        Log.e("TAG", "Msg: "+msg);
                        CommonUtilities.showCommonTextAlert(mContext, msg);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                else  {
                    CommonUtilities.showCommonTextAlert(mContext, "The system is temporarily unavailable. Please try again later");
                }
            }
            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                Log.e("TAG", "onFailure: "+t.getMessage());
                loadingDialog.dismiss();
            }
        });

    }
}