package com.daddydsbbq.Others;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;


import com.daddydsbbq.Activity.LoginActivity;
import com.daddydsbbq.R;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class CommonUtilities
{

    public static AlertDialog alertDialog;  // promptview






    //***************************************************************************//
    //Validation functions

    //Email check
    public static boolean isValidEmail(CharSequence email) {
        return email != null && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }


    public static void showCommonTextAlert(Context mContext, String msg) {
        LayoutInflater li = LayoutInflater.from(mContext);
        final View promptsView = li.inflate(R.layout.common_alert_layout, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mContext);
        // set prompts.xml to alertdialog builder
        alertDialogBuilder.setView(promptsView);
        final TextView titalText = promptsView
                .findViewById(R.id.alert_msg);
        titalText.setText(msg);
        promptsView.findViewById(R.id.btn_ok).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.cancel();
            }
        });
        // create alert dialog
        alertDialog = alertDialogBuilder.create();
        alertDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        alertDialog.show();


    }


    public static void showLoginAlert(final Context mContext)
    {
        LayoutInflater li = LayoutInflater.from(mContext);
        final View promptsView = li.inflate(R.layout.common_alert_layout2, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mContext);
        // set prompts.xml to alertdialog builder
        alertDialogBuilder.setView(promptsView);
        final TextView titalText = promptsView
                .findViewById(R.id.alert_msg);
        titalText.setText("Please login to continue. We can't let just anyone have this access to ordering!");


        promptsView.findViewById(R.id.btn_ok).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.cancel();
                SPUtilities.clearSharedPreference(mContext);
                Intent intent = new Intent(mContext, LoginActivity.class);
                mContext.startActivity(intent);
            }
        });

        promptsView.findViewById(R.id.btn_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.cancel();
            }
        });
        // create alert dialog
        alertDialog = alertDialogBuilder.create();
        alertDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        alertDialog.show();
    }


    //round decimal to 2
    public static String getFormatedDoubleValue(double value) {
        DecimalFormat numberFormatBtc = new DecimalFormat("#0.00");
        final String strValue = numberFormatBtc.format(value);
        return strValue;
    }


    public static String getCurrentTime()
    {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        String dateTime = simpleDateFormat.format(calendar.getTime()).toString();
        Log.e("Utilities - ", "Current Time: "+dateTime);

        return  dateTime;
    }
//    public static boolean showCommonTextAlertWithResponsee(Context mContext, String msg)
//    {
//        AlertDialog.Builder builder1 = new AlertDialog.Builder(mContext);
//        builder1.setMessage("Write your message here.");
//        builder1.setCancelable(true);
//
//        builder1.setPositiveButton(
//                "Yes",
//                new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int id) {
//                        dialog.cancel();
//
//                        return true;
//                    }
//                });
//
//        builder1.setNegativeButton(
//                "No",
//                new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int id) {
//                        dialog.cancel();
//                        return false;
//
//                    }
//                });
//
//        AlertDialog alert11 = builder1.create();
//        alert11.show();
//    }


    public static void getToast(final Context context, final String txt) {
        ((Activity)context).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                LayoutInflater inflater = ((Activity) context).getLayoutInflater();
                View layout = inflater.inflate(R.layout.toast_custom, (ViewGroup) ((Activity) context).findViewById(R.id.custom_toast_container));
                TextView text = layout.findViewById(R.id.text);
                text.setText(txt);
                Toast toast = new Toast(context);
                toast.setGravity(Gravity.CENTER, 0, 40);
                toast.setDuration(Toast.LENGTH_SHORT);
                toast.setView(layout);
                toast.show();
            }
        });
    }


    public static void sessionExpiredAlert(final Context mContext)
    {


        LayoutInflater li = LayoutInflater.from(mContext);
        final View promptsView = li.inflate(R.layout.common_alert_layout, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mContext);
        // set prompts.xml to alertdialog builder
        alertDialogBuilder.setView(promptsView);
        final TextView titalText = promptsView
                .findViewById(R.id.alert_msg);
        titalText.setText(Messages.SESSION_EXPIRED_MSG);
        promptsView.findViewById(R.id.btn_ok).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.cancel();
                SPUtilities.clearSharedPreference(mContext);
                Intent intent = new Intent(mContext, LoginActivity.class);
                mContext.startActivity(intent);
            }
        });
        // create alert dialog
        alertDialog = alertDialogBuilder.create();
        alertDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        alertDialog.show();
    }

    public void internetAlert(Context mContext)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle("No Internet Connection")
                .setMessage("");
        builder.setPositiveButton("Retry", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                ///handle here
            }
        });
        builder.show();
    }


    public static boolean isOnline(Context mContext) {
        ConnectivityManager cm =
                (ConnectivityManager) mContext.getSystemService(mContext.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null &&
                cm.getActiveNetworkInfo().isConnectedOrConnecting();
    }
}
