package com.daddydsbbq.Model.ResponseModel;

public class HomeMenuModel {

    private String category;
    private int category_id;
    private String category_url;

    public String getCategory_url() {
        return category_url;
    }

    public void setCategory_url(String category_url) {
        this.category_url = category_url;
    }
    // private boolean isChecked;

    public String getCategory_name() {
        return category;
    }

    public void setCategory_name(String category_name) {
        this.category = category_name;
    }

    public int getCategory_id() {
        return category_id;
    }

    public void setCategory_id(int category_id) {
        this.category_id = category_id;
    }
}

